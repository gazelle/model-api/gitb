package net.ihe.gazelle.lib.gitbprocessingclient.adapter;

import com.gitb.ps.Void;
import com.gitb.ps.*;
import com.github.tomakehurst.wiremock.WireMockServer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.ws.rs.core.MediaType;
import java.net.MalformedURLException;
import java.net.URL;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static org.junit.jupiter.api.Assertions.assertNotNull;


class GITBClientProcessImplTest {

    /**
     * tested class
     */
    private GITBClientProcessImpl gitbClientProcess;

    /**
     * mock server
     */
    private WireMockServer server = new WireMockServer(wireMockConfig().port(9999));

    private static final String RESPONSE_SHOULD_NOT_BE_NULL_MESSAGE = "response should not be null";

    private static final String SERVICE_PATH = "ProcessingServiceService";

    /**
     * init the mock server and tested class
     * @throws MalformedURLException if the url of the target is invlaid
     */
    @BeforeEach
    void init() throws MalformedURLException {
        server.start();
        server.stubFor(get(urlPathEqualTo("/" + SERVICE_PATH))
                .willReturn(aResponse()
                        .withStatus(200).withHeader("Content-Type", MediaType.APPLICATION_XML)
                        .withBodyFile("gitb_ps.wsdl")));
        server.stubFor(get(urlPathEqualTo("/gitb_core.xsd"))
                .willReturn(aResponse()
                        .withStatus(200).withHeader("Content-Type", MediaType.APPLICATION_XML)
                        .withBodyFile("gitb_core.xsd")));
        server.stubFor(get(urlPathEqualTo("/gitb_ps.xsd"))
                .willReturn(aResponse()
                        .withStatus(200).withHeader("Content-Type", MediaType.APPLICATION_XML)
                        .withBodyFile("gitb_ps.xsd")));
        server.stubFor(get(urlPathEqualTo("/gitb_tr.xsd"))
                .willReturn(aResponse()
                        .withStatus(200).withHeader("Content-Type", MediaType.APPLICATION_XML)
                        .withBodyFile("gitb_tr.xsd")));
        gitbClientProcess = new GITBClientProcessImpl(new URL("http://localhost:9999/" + SERVICE_PATH),
                "ProcessingServiceService", "ProcessingServicePort");
    }

    /**
     * reset the server
     */
    @AfterEach
    void stopServer() {
        server.resetAll();
        server.stop();
    }

    /**
     *test getModuleDefinition function
     */
    @Test
    void getModuleDefinitionTest() {
        stubSoapFromFile("gitb_dummy_module_response.xml");
        GetModuleDefinitionResponse actualResponse = gitbClientProcess.getModuleDefinition(new Void());
        assertNotNull(actualResponse, RESPONSE_SHOULD_NOT_BE_NULL_MESSAGE);
    }

    /**
     *test process function
     */
    @Test
    void processTest() {
        stubSoapFromFile("gitb_dummy_process_response.xml");
        ProcessResponse actualResponse = gitbClientProcess.process(new ProcessRequest());

        assertNotNull(actualResponse, RESPONSE_SHOULD_NOT_BE_NULL_MESSAGE);
    }

    /**
     *test beginTransaction function
     */
    @Test
    void beginTransactionTest() {
        stubSoapFromFile("gitb_dummy_transaction_response.xml");
        BeginTransactionResponse actualResponse = gitbClientProcess.beginTransaction(new BeginTransactionRequest());
        assertNotNull(actualResponse, RESPONSE_SHOULD_NOT_BE_NULL_MESSAGE);
    }

    /**
     *test endTransaction function
     */
    @Test
    void endTransactionTest() {
        stubSoapFromFile("gitb_dummy_end_transaction.xml");
        Void actualResponse = gitbClientProcess.endTransaction(new BasicRequest());
        assertNotNull(actualResponse, RESPONSE_SHOULD_NOT_BE_NULL_MESSAGE);
    }


    /**
     * stub the server from file
     * @param path the file path
     */
    private void stubSoapFromFile(String path) {
        server.stubFor(post(urlPathEqualTo("/" + SERVICE_PATH))
                .willReturn(aResponse()
                        .withStatus(200).withHeader("Content-Type", MediaType.APPLICATION_XML)
                        .withBodyFile(path)));
    }


}