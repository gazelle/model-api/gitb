package net.ihe.gazelle.lib.gitbprocessingclient.adapter;


import com.gitb.ps.Void;
import com.gitb.ps.*;

import javax.xml.namespace.QName;
import java.net.URL;

/**
 * Client for GITB processing service
 *
 * @author Luc Chatty
 */
public class GITBClientProcessImpl implements ProcessingService {

    /**
     * the processing service to point to
     */
    private ProcessingService service;

    /**
     * Client constructor with associated server url
     *
     * @param endPoint    the url of the server implementing the processing service
     * @param serviceName name of the ProcessingServiceService service
     * @param portName    name of the port of the ProcessingServiceService
     */
    public GITBClientProcessImpl(URL endPoint, String serviceName, String portName) {
        ProcessingServiceService serviceHandler = new ProcessingServiceService(endPoint,
                new QName("http://www.gitb.com/ps/v1/", serviceName));
        service = serviceHandler.getPort(new QName("http://www.gitb.com/ps/v1/", portName), ProcessingService.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public GetModuleDefinitionResponse getModuleDefinition(Void parameters) {
        return service.getModuleDefinition(parameters);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ProcessResponse process(ProcessRequest processRequest) {
        return service.process(processRequest);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BeginTransactionResponse beginTransaction(BeginTransactionRequest parameters) {
        return service.beginTransaction(parameters);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Void endTransaction(BasicRequest parameters) {
        return service.endTransaction(parameters);
    }

}
