package net.ihe.gazelle.lib.gitbmessagingclient.adapter;

import com.gitb.ms.BasicRequest;
import com.gitb.ms.BeginTransactionRequest;
import com.gitb.ms.FinalizeRequest;
import com.gitb.ms.GetModuleDefinitionResponse;
import com.gitb.ms.InitiateRequest;
import com.gitb.ms.InitiateResponse;
import com.gitb.ms.ReceiveRequest;
import com.gitb.ms.SendRequest;
import com.gitb.ms.SendResponse;
import com.gitb.ms.Void;
import com.github.tomakehurst.wiremock.WireMockServer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.ws.rs.core.MediaType;
import java.net.MalformedURLException;
import java.net.URL;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static org.junit.jupiter.api.Assertions.assertNotNull;


class GITBClientMessagingImplTest {

    /**
     * tested class
     */
    private GITBClientMessagingImpl gitbClientMessaging;

    /**
     * mock server
     */
    private WireMockServer server = new WireMockServer(wireMockConfig().port(9999));

    private static final String RESPONSE_SHOULD_NOT_BE_NULL_MESSAGE = "response should not be null";

    private static final String SERVICE_PATH = "MessagingServiceService";

    /**
     * init the mock server and tested class
     * @throws MalformedURLException if the url of the target is invlaid
     */
    @BeforeEach
    void init() throws MalformedURLException {
        server.start();
        server.stubFor(get(urlPathEqualTo("/" + SERVICE_PATH))
                .willReturn(aResponse()
                        .withStatus(200).withHeader("Content-Type", MediaType.APPLICATION_XML)
                        .withBodyFile("gitb_ms.wsdl")));
        server.stubFor(get(urlPathEqualTo("/gitb_core.xsd"))
                .willReturn(aResponse()
                        .withStatus(200).withHeader("Content-Type", MediaType.APPLICATION_XML)
                        .withBodyFile("gitb_core.xsd")));
        server.stubFor(get(urlPathEqualTo("/gitb_ms.xsd"))
                .willReturn(aResponse()
                        .withStatus(200).withHeader("Content-Type", MediaType.APPLICATION_XML)
                        .withBodyFile("gitb_ms.xsd")));
        server.stubFor(get(urlPathEqualTo("/gitb_tr.xsd"))
                .willReturn(aResponse()
                        .withStatus(200).withHeader("Content-Type", MediaType.APPLICATION_XML)
                        .withBodyFile("gitb_tr.xsd")));
        gitbClientMessaging = new GITBClientMessagingImpl(new URL("http://localhost:9999/"+ SERVICE_PATH + "?wsdl"));
    }

    /**
     * reset the server
     */
    @AfterEach
    void stopServer() {
        server.resetAll();
        server.stop();
    }

    /**
     *test getModuleDefinition function
     */
    @Test
    void getModuleDefinitionTest() {
        stubSoapFromFile("gitb_dummy_module_response.xml");

        GetModuleDefinitionResponse actualResponse = gitbClientMessaging.getModuleDefinition(new Void());

        assertNotNull(actualResponse, RESPONSE_SHOULD_NOT_BE_NULL_MESSAGE);
    }


    /**
     *test initiate function
     */
    @Test
    void initiateTest() {
        stubSoapFromFile("gitb_dummy_initiate_response.xml");

        InitiateResponse actualResponse = gitbClientMessaging.initiate(new InitiateRequest());

        assertNotNull(actualResponse, RESPONSE_SHOULD_NOT_BE_NULL_MESSAGE);
    }

    /**
     *test receive function
     */
    @Test
    void receiveTest() {
        stubSoapFromFile("gitb_dummy_receive_response.xml");

        Void actualResponse = gitbClientMessaging.receive(new ReceiveRequest());

        assertNotNull(actualResponse, RESPONSE_SHOULD_NOT_BE_NULL_MESSAGE);
    }

    /**
     *test send function
     */
    @Test
    void sendTest() {
        stubSoapFromFile("gitb_dummy_send_response.xml");

        SendResponse actualResponse = gitbClientMessaging.send(new SendRequest());

        assertNotNull(actualResponse, RESPONSE_SHOULD_NOT_BE_NULL_MESSAGE);
    }

    /**
     *test beginTransaction function
     */
    @Test
    void beginTransactionTest() {
        stubSoapFromFile("gitb_dummy_transaction_response.xml");

        Void actualResponse = gitbClientMessaging.beginTransaction(new BeginTransactionRequest());

        assertNotNull(actualResponse, RESPONSE_SHOULD_NOT_BE_NULL_MESSAGE);
    }

    /**
     *test endTransaction function
     */
    @Test
    void endTransactionTest() {
        stubSoapFromFile("gitb_dummy_end_transaction.xml");

        Void actualResponse = gitbClientMessaging.endTransaction(new BasicRequest());

        assertNotNull(actualResponse, RESPONSE_SHOULD_NOT_BE_NULL_MESSAGE);
    }

    /**
     *test finalize function
     */
    @Test
    void finalizeTest() {
        stubSoapFromFile("gitb_dummy_finalize_response.xml");

        Void actualResponse = gitbClientMessaging.finalize(new FinalizeRequest());

        assertNotNull(actualResponse, RESPONSE_SHOULD_NOT_BE_NULL_MESSAGE);
    }

    /**
     * stub the server from file
     * @param path the file path
     */
    private void stubSoapFromFile(String path) {
        server.stubFor(post(urlPathEqualTo("/" + SERVICE_PATH))
                .willReturn(aResponse()
                        .withStatus(200).withHeader("Content-Type", MediaType.APPLICATION_XML)
                        .withBodyFile(path)));
    }


}