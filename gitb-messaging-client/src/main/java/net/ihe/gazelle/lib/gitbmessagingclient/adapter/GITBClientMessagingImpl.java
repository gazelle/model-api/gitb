package net.ihe.gazelle.lib.gitbmessagingclient.adapter;


import com.gitb.ms.BasicRequest;
import com.gitb.ms.BeginTransactionRequest;
import com.gitb.ms.FinalizeRequest;
import com.gitb.ms.GetModuleDefinitionResponse;
import com.gitb.ms.InitiateRequest;
import com.gitb.ms.InitiateResponse;
import com.gitb.ms.MessagingService;
import com.gitb.ms.MessagingServiceService;
import com.gitb.ms.ReceiveRequest;
import com.gitb.ms.SendRequest;
import com.gitb.ms.SendResponse;
import com.gitb.ms.Void;

import java.net.URL;

/**
 * Client for GITB messaging service
 * @author Luc Chatty
 */
public class GITBClientMessagingImpl implements MessagingService {

    /**
     * the messaging service to point to
     */
    private MessagingService service;

    /**
     * Client constructor with associated server url
     * @param endPoint the url of the server implementing the messaging service
     */
    public GITBClientMessagingImpl(URL endPoint) {
        MessagingServiceService serviceHandler = new MessagingServiceService(endPoint);
        service = serviceHandler.getMessagingServicePort();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public GetModuleDefinitionResponse getModuleDefinition(Void aVoid) {
        return service.getModuleDefinition(aVoid);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InitiateResponse initiate(InitiateRequest initiateRequest) {
        return service.initiate(initiateRequest);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Void receive(ReceiveRequest receiveRequest) {
        return service.receive(receiveRequest);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SendResponse send(SendRequest sendRequest) {
        return service.send(sendRequest);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Void beginTransaction(BeginTransactionRequest beginTransactionRequest) {
        return service.beginTransaction(beginTransactionRequest);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Void endTransaction(BasicRequest basicRequest) {
        return service.endTransaction(basicRequest);
    }

    // We suppress warning on "The signature of "finalize()" should match that of "Object.finalize()"
    // we need to follow the interface but it's dangerous
    @java.lang.SuppressWarnings("java:S1175")
    @Override
    public Void finalize(FinalizeRequest finalizeRequest) {
        return service.finalize(finalizeRequest);
    }
}
