# Gazelle GITB Library

## Introduction

### What is GITB?

GITB (Global Interoperability Test Bed) is a standardization project of the CEN (_European Committee
 for Standardization_) that defines methodologies and architecture to support e-business standard 
 assessment, conformance and interoperability testing.
 
Here are the official GITB documentation of 
[GITB Services](https://www.itb.ec.europa.eu/docs/services/latest/) and 
[GITB Test Definition Language](https://www.itb.ec.europa.eu/docs/tdl/latest/)

### What is this library?

This project is a **library** that provides elements for using and implemententing GITB
 services in the Gazelle platform.
 
So far two Java components are available : **GITB Messaging Client** and **GITB Processing Client**
that can preform a SOAP webservice call respectively to GITB messaging or processing service.

## How to use

### Messaging client

To use the messaging client, add the following dependency to your project `pom.xml` :

```xml
<dependency>
    <groupId>net.ihe.gazelle</groupId>
    <artifactId>lib.gitb-messaging-client</artifactId>
    <version>...</version>
</dependency>
```

You can then in your java code instantiate the client with such code : 

```java
public class MyClient {
    private MessagingService messagingClient;

    public MyClient(URL messagingServiceURL){
        this.messagingClient =  new GITBClientMessagingImpl(messagingServiceURL);
    }
    
    public MyResponse send(MyRequest myRequest){
        
        // Process to transform your request to GITB send request
        SendRequest sendRequest = transformToGITBRequest(myRequest);  
       
        SendResponse sendResponse = this.messagingClient.send(sendRequest);
        
        //Process to transform the GITB Response into your own response object
        return transformToBusinessResponse(sendResponse);
    }
    
}
```

The URL used to instantiate the client will be the endpoint to you Messaging Service. You can then used all methods defined 
by GITB directly on the Client object.

### Processing Client

To use the messaging client, add the following dependency to your project `pom.xml` :

```xml
<dependency>
    <groupId>net.ihe.gazelle</groupId>
    <artifactId>lib.gitb-processing-client</artifactId>
    <version>...</version>
</dependency>
```

You can then in your java code instantiate the client with such code : 

```java
public class MyClient {
    private ProcessingService processingClient;

    public MyClient(URL processingServiceURL){
        this.processingClient =  new GITBClientProcessImpl(processingServiceURL);
    }
    
    public MyResponse send(MyRequest myRequest){
        
        // Process to transform your request to GITB send request
        ProcessRequest processRequest = transformToGITBRequest(myRequest);  
       
        ProcessResponse processResponse = this.processingClient.process(processRequest);
        
        //Process to transform the GITB Response into your own response object
        return transformToBusinessResponse(processResponse);
    }
    
}
```

The URL used to instantiate the client will be the endpoint to you Processing Service. You can then used all methods defined 
by GITB directly on the Client object.


### AnyContent Mapper

The Gazelle GITB Mapper is providing a mapper to automatically transform any object as GITB
 AnyContent and AnyContent to objects. This mapper is using JAVA Reflective API to access object
  members and constructors to read / write fields of the object.
  
When transforming back from AnyContent to objects, the mapper need to instantiate the
 object class. To do so, the mapper need the class to:
 
 1. Either have a constructor with no arguments (can be visibility `package`)
 1. Either have a constructor that have all arguments name's matching one field name (not all fields
  are required to be arguments in the constructor) __AND__ the object must have been compiled with
   the `-parameters` option to keep arguments' name in the compiled class files.

Example of `pom.xml` declaration to have the compiler option

```xml
<project>
    [...]
        <build>
        [...]
            <plugins>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-compiler-plugin</artifactId>
                    <version>3.8.1</version>
                    <configuration>
                        <compilerArgs>
                            <arg>-parameters</arg>
                        </compilerArgs>
                    </configuration>
                </plugin>
            </plugins>
        [...]
        </build>
    [...]
</project>
```

The mapper supports the following Java Classes : 
* `String`
* `int`, `Integer`
* `long`, `Long`
* `float`, `Float`
* `double`, `Double`
* `boolean`, `Boolean`
* `Date`
* `List\<?\>`

In addition to those classes, the Mapper supports the types:
* concrete __class__ (with inheritance)
* __enumeration__.

If class members are interfaces or abstract classes, the Mapper will fail to decode them.

Supported GITB Types are : 
* `string`
* `number`
* `boolean`
* `object`
* `list`

Not yet supported Java classes :
* `byte[]`, `Byte[]`
* `URL`
* `Set\<?\>`
* `Map\<?\>`

Not yet supported GITB Types are :
* `binary`
* `map`


To use the automatic mapping in your project, simply add to your `pom.xml` : 

```xml
<dependency>
    <groupId>net.ihe.gazelle</groupId>
    <artifactId>lib.gitb-utils</artifactId>
    <version>...</version>
</dependency>
```

Then you can use the two mapper classes : `MapperAnyContentToObject` and `MapperObjectToAnyContent`.
They define the following methods to map any supported type to an AnyContent Object or, the other way around,
AnyContent objects to Java classes :

```java
public class MapperAnyContentToObject {

   // Get an object T from an AnyContent Object. See javadoc for more details.
   public <T> T getObject(AnyContent anyContent, Class<T> tClass) throws MappingException {
       //...
   }

   // Get a Collection of object E from an AnyContent Object. See javadoc for more details.
   public <L extends List<?>, E> L getObjectCollection(AnyContent anyContent, Class<L> listClass, Class<E> elementClass) throws MappingException {
       //...
   }
}

public class MapperObjectToAnyContent {

   // Map any Object to and AnyContent. See javadoc for more details.
   public <T> AnyContent getAnyContent(String name, T object) throws MappingException {
       //...
   }
}
```

Those mappers can be instantiated in your project to map any object to AnyContent or the opposite : 

```java
public class MyClass {
    public AnyContent toAnyContent(MyObject myObject){
        MapperObjectToAnyContent mapper = new MapperObjectToAnyContent();
        try {
            return mapper.getAnyContent("My Object", myObject);
        } catch (MappingException e){
            // Process the exception
        }
    }
    
    public MyObject toMyObject(AnyContent myAnyContent){
        MapperAnyContentToObject mapper = new MapperAnyContentToObject();
        try {
            return mapper.getObject(myAnyContent, MyObject.class);
        } catch (MappingException e){
            // Process the exception
        }
    }

    public List<MyObject> toListOfMyObject(AnyContent myAnyContent){
        MapperAnyContentToObject mapper = new MapperAnyContentToObject();
        try {
            return mapper.getObjectCollection(myAnyContent, List.class, MyObject.class);
        } catch (MappingException e){
            // Process the exception
        }
    }
}
```
