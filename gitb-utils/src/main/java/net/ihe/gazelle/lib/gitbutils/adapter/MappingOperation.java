package net.ihe.gazelle.lib.gitbutils.adapter;

import com.gitb.core.ValueEmbeddingEnumeration;
import net.ihe.gazelle.lib.annotations.Package;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Enumeration of possible mapping operation. It groups GITB type and classes together depending on the mapping methods.
 *
 * @author wbars, ceoche
 */
@Package
enum MappingOperation {

   SIMPLE(ValueEmbeddingEnumeration.STRING, String.class, int.class, Integer.class, long.class, Long.class, float.class, Float.class, double.class,
         Double.class, boolean.class, Boolean.class),
   BINARY(ValueEmbeddingEnumeration.BASE_64, byte[].class, Byte[].class),
   SCHEMA(ValueEmbeddingEnumeration.URI),
   MAP(null, Map.class),
   LIST(null, List.class, Set.class),
   DATE(ValueEmbeddingEnumeration.STRING, Date.class),
   OBJECT(null, Object.class),
   ENUM(ValueEmbeddingEnumeration.STRING);

   private ValueEmbeddingEnumeration embeddingMethod;
   private List<Class<?>> classes = new ArrayList<>();

   /**
    * Enumeration constructor
    *
    * @param embeddingMethod GITB Embedding memthod
    * @param classes         Associated Java classes for this mapping operation
    */
   private MappingOperation(ValueEmbeddingEnumeration embeddingMethod, Class<?>... classes) {
      this.embeddingMethod = embeddingMethod;
      this.classes.addAll(Arrays.asList(classes));
   }

   /**
    * Get the Embedding Method
    *
    * @return the Embedding Method
    */
   public ValueEmbeddingEnumeration getEmbeddingMethod() {
      return embeddingMethod;
   }

   /**
    * Get the asociated mapping operation of a Java Class
    *
    * @param classInstance Class to get the associated mapping operation
    *
    * @return the mapping operation associated with the given class or null if none can be associated.
    */
   public static MappingOperation getFromClass(Class<?> classInstance) {
      if (classInstance.isEnum()) {
         return ENUM;
      }
      for (MappingOperation mappingOperation : MappingOperation.values()) {
         if (!mappingOperation.classes.isEmpty()) {
            for (Class<?> clazz : mappingOperation.classes) {
               if (clazz.isAssignableFrom(classInstance)) {
                  return mappingOperation;
               }
            }
         }
      }
      return null;
   }

}
