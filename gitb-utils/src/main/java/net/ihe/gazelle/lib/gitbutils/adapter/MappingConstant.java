package net.ihe.gazelle.lib.gitbutils.adapter;

import net.ihe.gazelle.lib.annotations.Package;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * This class contains constants used for mapping.
 */
@Package
class MappingConstant {

   public static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
   public static final Charset DEFAULT_ENCODING = StandardCharsets.UTF_8;

   /**
    * Private default constructor to hide public one.
    */
   private MappingConstant() {
   }

}
