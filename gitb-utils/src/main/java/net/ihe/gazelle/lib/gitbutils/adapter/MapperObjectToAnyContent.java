package net.ihe.gazelle.lib.gitbutils.adapter;

import com.gitb.core.AnyContent;
import com.gitb.core.ValueEmbeddingEnumeration;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

/**
 * Generic mapper allowing to transform any object into AnyContent to feed GITB services.
 *
 * @author wbars, ceoche
 */
public class MapperObjectToAnyContent {

   /**
    * Public method exposed by the mapper to create an {@link AnyContent} from any Object instance.
    *
    * @param name   : name to give to the root {@link AnyContent}
    * @param object : object instance to map to {@link AnyContent}
    * @param <T>    : Class of the object to map
    *
    * @return mapped {@link AnyContent}
    *
    * @throws MappingException : if the object cannot be mapped to AnyContent
    */
   public <T> AnyContent getAnyContent(String name, T object) throws MappingException {
      if (object != null) {
         Class objectClass = object.getClass();
         return createAnyContentFromClass(name, objectClass, object);
      } else {
         throw new MappingException("Cannot map null to AnyContent");
      }
   }

   /**
    * Creates an {@link AnyContent} object from a {@link Class} and one of its instance
    *
    * @param objectName    : name to give to the root {@link AnyContent}
    * @param objectClass   : Class of the object to map
    * @param classInstance : object instance to map to {@link AnyContent}
    *
    * @return mapped {@link AnyContent}
    *
    * @throws MappingException : if the object cannot be mapped to AnyContent
    */
   private AnyContent createAnyContentFromClass(String objectName, Class objectClass, Object classInstance) throws MappingException {
      if (classInstance != null) {
         MappingOperation mappingOperation = MappingOperation.getFromClass(objectClass);
         if (mappingOperation == null) {
            throw new MappingException(String.format("Unsupported object class %s for AnyContent mapping.", objectClass));
         }
         switch (mappingOperation) {
            case SIMPLE:
               return createAnyContentSimple(objectName, GITBAnyContentType.getFromClass(objectClass).getGitbType(), classInstance.toString(),
                     ValueEmbeddingEnumeration.STRING);
            case LIST:
               if (!((Collection) classInstance).isEmpty()){
                  AnyContent listAnyContent = createAnyContentSimple(objectName, GITBAnyContentType.getFromClass(objectClass).getGitbType(), null
                          , null);
                  for (Object listItem : (Collection) classInstance) {
                     listAnyContent.getItem().add(createAnyContentFromClass(':' + listItem.getClass().getCanonicalName(), listItem.getClass(), listItem));
                  }
                  return listAnyContent;
               } else {
                  return null;
               }
            case DATE:
               SimpleDateFormat dateFormat = new SimpleDateFormat(MappingConstant.DATE_FORMAT);
               return createAnyContentSimple(objectName, GITBAnyContentType.getFromClass(objectClass).getGitbType(),
                     dateFormat.format((Date) classInstance), ValueEmbeddingEnumeration.STRING);
            case OBJECT:
               return createAnyContentForObject(objectName, objectClass, classInstance);
            case ENUM:
               return createAnyContentSimple(objectName, GITBAnyContentType.STRING.getGitbType(), ((Enum) classInstance).name(),
                     ValueEmbeddingEnumeration.STRING);
            default:
               throw new MappingException(String.format("Unsupported AnyContent type %s for AnyContent mapping.", mappingOperation));
         }
      } else {
         return null;
      }
   }

   /**
    * Creates an {@link AnyContent} Object from an Object that is not a simple type, a date or a collection.
    *
    * @param objectName    : name to give to the root {@link AnyContent}
    * @param objectClass   : Class of the object to map
    * @param classInstance : object instance to map to {@link AnyContent}
    *
    * @return mapped {@link AnyContent}
    *
    * @throws MappingException : if the object cannot be mapped to AnyContent
    */
   private AnyContent createAnyContentForObject(String objectName, Class objectClass, Object classInstance) throws MappingException {
      AnyContent objectAnyContent = createAnyContentSimple(objectName, GITBAnyContentType.OBJECT.getGitbType(), null, null);
      addAnyContentForFields(objectClass, classInstance, objectAnyContent);
      Class superClass = objectClass.getSuperclass();
      while (superClass != null) {
         addAnyContentForFields(superClass, classInstance, objectAnyContent);
         superClass = superClass.getSuperclass();
      }
      return objectAnyContent;
   }

   /**
    * Creates an {@link AnyContent} object for each valued field of the given class instance. Add them to the given {@link AnyContent} corresponding
    * to the Object.
    *
    * @param objectClass      : Class of the object to map
    * @param classInstance    : object instance to map to {@link AnyContent}
    * @param objectAnyContent : {@link AnyContent} object corresponding to the object containing the fields.
    *
    * @throws MappingException : if the object cannot be mapped to AnyContent
    */
   private void addAnyContentForFields(Class objectClass, Object classInstance, AnyContent objectAnyContent) throws MappingException {
      Field[] fields = objectClass.getDeclaredFields();
      for (Field field : fields) {
         AnyContent fieldAnyContent = createAnyContentFromField(field, classInstance);
         if (fieldAnyContent != null) {
            objectAnyContent.getItem().add(fieldAnyContent);
         }
      }
   }

   /**
    * Creates an {@link AnyContent} object for a specific field.
    *
    * @param field      : field to map to {@link AnyContent}
    * @param fieldOwner : Owner of the field to retrieve its value.
    *
    * @return mapped {@link AnyContent}
    *
    * @throws MappingException : if the object cannot be mapped to AnyContent
    */
   private AnyContent createAnyContentFromField(Field field, Object fieldOwner) throws MappingException {
      try {
         if (!(Modifier.isStatic(field.getModifiers()))) {
            field.setAccessible(true);
            if(field.get(fieldOwner)!=null){
               Class fieldValueClass = field.get(fieldOwner).getClass();
               AnyContent anyContent = createAnyContentFromClass(getNameForField(field, fieldValueClass), fieldValueClass, field.get(fieldOwner));
               field.setAccessible(false);
               return anyContent;
            }
            field.setAccessible(false);
         }
         return null;
      } catch (IllegalAccessException e) {
         throw new MappingException(String.format("Cannot access field %s of class %s.", field.getName(), fieldOwner.getClass()));
      }
   }

   /**
    * Get the name given to the AnyContent for a specific Field.
    * If the value of the field is of the exact same type as the field, name will be the name of the field.
    * If the value of the field is an instance of a sub-class of the fields type, the name will be suffixed with the Canonical name of the sub-class.
    * eg :     name
    *          name:net.ihe.gazelle.lib.mypackage.business.MyClass
    *
    * @param field               Field to map
    * @param fieldValueClass     Actual class of the Fields value
    * @return the literal value of the name to give to mapped {@link AnyContent}
    */
   private String getNameForField(Field field, Class fieldValueClass){
      if (field.getType().equals(fieldValueClass) ||
              (!MappingOperation.getFromClass(fieldValueClass).equals(MappingOperation.OBJECT) &&
                      !MappingOperation.getFromClass(fieldValueClass).equals(MappingOperation.ENUM))) {
         return field.getName();
      }
      return field.getName() + ':' + fieldValueClass.getCanonicalName();
   }

   /**
    * Create a AnyContent object value based on the provided parameters.
    *
    * @param name            The name of the value.
    * @param value           The value itself.
    * @param embeddingMethod The way in which this value is to be considered.
    *
    * @return The value.
    */
   private AnyContent createAnyContentSimple(String name, String type, String value, ValueEmbeddingEnumeration embeddingMethod) {
      AnyContent input = new AnyContent();
      input.setName(name);
      if (value != null) {
         input.setValue(value);
      }
      if (type != null) {
         input.setType(type);
      } else {
         throw new IllegalArgumentException("Cannot create AnyContent Element without a type !");
      }
      if (embeddingMethod != null) {
         input.setEmbeddingMethod(embeddingMethod);
      }
      return input;
   }
}
