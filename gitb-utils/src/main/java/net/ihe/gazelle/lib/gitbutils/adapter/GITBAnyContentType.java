package net.ihe.gazelle.lib.gitbutils.adapter;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Types supported by GITB AnyContent. This enumeration is not defined in the GITB XSD.
 *
 * @author ceoche
 */
public enum GITBAnyContentType {

   STRING("string", String.class, Date.class, URL.class),
   NUMBER("number", int.class, Integer.class, long.class, Long.class, float.class, Float.class, double.class, Double.class),
   BOOLEAN("boolean", boolean.class, Boolean.class),
   BINARY("binary", byte[].class, Byte[].class),
   SCHEMA("schema"),
   MAP("map", Map.class),
   LIST("list", Collection.class),
   OBJECT("object", Object.class);

   private String gitbType;
   private List<Class<?>> javaClasses = new ArrayList<>();

   /**
    * Default constructor for the class.
    * @param gitbType    literal value of the GITB type of {@link com.gitb.core.AnyContent}
    * @param javaClasses classes associated to given GITB type.
    */
   GITBAnyContentType(String gitbType, Class<?>... javaClasses) {
      this.gitbType = gitbType;
      this.javaClasses.addAll(Arrays.asList(javaClasses));
   }

   /**
    * Get the literal value of the GITB Type
    *
    * @return a String of the GITB Type
    */
   public String getGitbType() {
      return this.gitbType;
   }

   /**
    * Get the list of Java classes that can be used by the Mapper to transform the GITB AnyContent Type.
    *
    * @return a list of Java classes
    */
   public List<Class<?>> getJavaClasses() {
      return new ArrayList<>(javaClasses);
   }

   /**
    * Get the asociated GITB AnyContent Type of a Java Class
    *
    * @param tClass Class to get the associated type
    *
    * @return the GITB AnyContent Type associated with the given class or null if none can be associated.
    */
   public static GITBAnyContentType getFromClass(Class<?> tClass) {
      if (tClass.isEnum()) {
         return STRING;
      }
      for (GITBAnyContentType anyContentType : GITBAnyContentType.values()) {
         for (Class<?> clazz : anyContentType.javaClasses) {
            if (clazz.isAssignableFrom(tClass)) {
               return anyContentType;
            }
         }
      }
      return null;
   }

   /**
    * Get the GITBAnyContentType from the GITB type values.
    *
    * @param value literal value of the GITB Type
    *
    * @return the GITBAnyContentType or null if none can be associated.
    */
   public static GITBAnyContentType getFromAnyContentType(String value) {
      return GITBAnyContentType.valueOf(value.toUpperCase());
   }

}
