package net.ihe.gazelle.lib.gitbutils.adapter;

import com.gitb.core.AnyContent;

import java.lang.reflect.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Can be used to decode GITB AnyContent structure as Object. It performs the reverse operation of the {@link MapperObjectToAnyContent}
 *
 * @author ceoche, nbaillet
 */
public class MapperAnyContentToObject {

    private DateFormat dateFormat = new SimpleDateFormat(MappingConstant.DATE_FORMAT);

    /**
     * <p>Transform {@link AnyContent} as any object using the given class. This method performs the invers transformation of {@link
     * MapperObjectToAnyContent#getAnyContent(String, Object)}. The Mapper will explore the given AnyContent and by reflection will instantiate the
     * class and feed all fields.
     * </p>
     * <p>Supported Java Types are:</p>
     * <ul>
     * <li>{@link String}</li>
     * <li>int, {@link Integer}</li>
     * <li>long, {@link Long}</li>
     * <li>float, {@link Float}</li>
     * <li>double, {@link Double}</li>
     * <li>boolean, {@link Boolean}</li>
     * <li>{@link Date}</li>
     * <li>{@link Object}</li>
     * </ul>
     * <p>In addition, the mapper supports only <strong>concrete class</strong> and <strong>enumeration</strong>. If class members are interfaces or
     * abstract classes, the mapper will
     * fail to decode them</p>
     *
     * <p>If the root node of the AnyContent is a {@link List}, the methode {@link #getObjectCollection(AnyContent, Class, Class)} should instead be
     * called.</p>
     *
     * @param anyContent AnyContent instance to transform as object
     * @param tClass     Class of the targeted object to transform. The class must reflect the structure of the AnyContent instance and must have
     *                   either a constructor without arguments, either a constructor with parameters those have the same name than the fields they
     *                   relates to.
     * @param <T>        The targeted object type
     * @return an instance of the given class fill with the values of the AnyContent element.
     * @throws MappingException if the reverse mapping cannot be done. The reasons can be that the class is different from the AnyContent
     *                          structure, or
     *                          one of the fields is using a type not supported, or the given class is not instantiable.
     */
    public <T> T getObject(AnyContent anyContent, Class<T> tClass) throws MappingException {

        GITBAnyContentType expectedAnyContentType = GITBAnyContentType.getFromClass(tClass);
        MappingOperation mappingOperation = MappingOperation.getFromClass(tClass);

        if (isTypeCorrect(anyContent, expectedAnyContentType)) {
            if (isEmbeddingMethodCorrect(anyContent, mappingOperation)) {
                switch (mappingOperation) {
                    case SIMPLE:
                        return decodeTypeSimple(anyContent, tClass);
                    case DATE:
                        return decodeDate(anyContent);
                    case ENUM:
                        return decodeEnum(anyContent, tClass);
                    case OBJECT:
                        return decodeObject(anyContent, tClass);
                    default:
                        throw new UnsupportedOperationException(
                                String.format("%s Single Mapping operation is not implemented. Relates to GITB AnyContent [name=%s, type=%s]",
                                        mappingOperation.name(), anyContent.getName(), anyContent.getType()));
                }
            } else {
                throw new MappingException(
                        String.format("Unable to decode GITB AnyContent [name=%s, type=%s] with the requested embeddingMethod %s",
                                anyContent.getName(),
                                anyContent.getType(), anyContent.getEmbeddingMethod().name()));
            }
        } else {
            throw new MappingException(
                    String.format("Unable to transform GITB AnyContent [name=%s, type=%s] into the requested Java Class %s", anyContent.getName(),
                            anyContent.getType(), tClass.getCanonicalName()));
        }
    }

    /**
     * <p>Transform {@link AnyContent} as a collection using the given class. This method performs the invers transformation of {@link
     * MapperObjectToAnyContent#getAnyContent(String, Object)}. This method must only be called if the root element of the AnyContent is of type
     * <strong>list</strong>, otherwise {@link #getObject(AnyContent, Class)} must be called.</p>
     * <p>The Mapper will explore the given AnyContent and by reflection will instantiate the class and feed all fields.</p>
     * <p> Supported Java Types are:</p>
     * <ul>
     * <li>{@link List}&lt;?&gt;</li>
     * </ul>
     *
     * @param anyContent   AnyContent instance to transform as object collection
     * @param listClass    Class of the targeted list to transform. It can be an implementation of the {@link List} interface or the {@link List}
     *                     interface itself. If this is the interface, the mapper will instantiate an {@link ArrayList}.
     * @param elementClass Class of the targeted item of the list to transform. The class must reflect the structure of the AnyContent item and must
     *                     have either a constructor without arguments, either a constructor with parameters those have the same name than the fields
     *                     they relates to.
     * @param <L>          The targeted collection type
     * @param <E>          The targeted type of the item's list
     * @return an instance of the given class fill with the values of the AnyContent element.
     * @throws MappingException if the reverse mapping cannot be done. The reasons can be that the class is different from the AnyContent
     *                          structure, or
     *                          one of the fields is using a type not supported.
     */
    public <L extends List<?>, E> L getObjectCollection(AnyContent anyContent, Class<L> listClass, Class<E> elementClass) throws MappingException {
        MappingOperation mappingOperation = MappingOperation.getFromClass(listClass);
        if (isTypeCorrect(anyContent, GITBAnyContentType.getFromClass(listClass))) {
            switch (mappingOperation) {
                case LIST:
                    return decodeList(anyContent, listClass, elementClass);
                case MAP:
                default:
                    throw new UnsupportedOperationException(
                            String.format("%s Collection Mapping operation is not implemented for GITB AnyContent [name=%s, type=%s]",
                                    mappingOperation.name(), anyContent.getName(), anyContent.getType()));
            }
        } else {
            throw new MappingException(
                    String.format("Unable to transform GITB AnyContent [name=%s, type=%s] into the requested Java Class %s", anyContent.getName(),
                            anyContent.getType(), listClass.getCanonicalName()));
        }
    }

    /**
     * Analyze the given {@link Field} and {@link AnyContent} and determine if this is a collection or not and then call {@link #getObject(AnyContent,
     * Class)} or {@link #getObjectCollection(AnyContent, Class, Class)}
     *
     * @param anyContent AnyContent to transform as field value
     * @param field      Field matching the given AnyContent from which Type information will be extracted
     * @return an instance object suitable for the given field and fill with the values of the AnyContent element.
     * @throws MappingException If the AnyContent cannot be transformed as object.
     */
    private Object getAnyObjectForField(AnyContent anyContent, Field field) throws MappingException {
        Class<?> fClass = field.getType();
        if (List.class.isAssignableFrom(fClass)) {
            return getObjectCollection(anyContent, (Class<? extends List<?>>) fClass,
                    (Class<?>) ((ParameterizedType) field.getGenericType()).getActualTypeArguments()[0]);
        } else {
            if (canBeAssignedToFieldOrParameter(anyContent, field.getName(), fClass)) {
                Class anyContentClass = getClassFromAnyContent(anyContent);
                return getObject(anyContent, anyContentClass != null ? anyContentClass : fClass);
            } else {
                throw new MappingException(String.format("AnyContent element for field %s of class %s cannot be mapped ! " +
                        "Its type is not assignable to the field !", field.getName(), field.getDeclaringClass()));
            }
        }
    }

    /**
     * Analyse the given constructor {@link Parameter} and {@link AnyContent} and determine if this is a collection or not and then call {@link
     * #getObject(AnyContent, Class)} or {@link #getObjectCollection(AnyContent, Class, Class)}
     *
     * @param anyContent AnyContent to transform as constructor parameter value
     * @param parameter  Parameter matching the given AnyContent from which Type information will be extracted
     * @return an instance object suitable for the given parameter and fill with the values of the AnyContent element.
     * @throws MappingException If the AnyContent cannot be transformed as object.
     */
    private Object getAnyObjectForConstructorArgument(AnyContent anyContent, Parameter parameter) throws MappingException {
        Class<?> pClass = parameter.getType();
        if (List.class.isAssignableFrom(pClass)) {
            return getObjectCollection(anyContent, (Class<? extends List<?>>) pClass,
                    (Class<?>) ((ParameterizedType) parameter.getParameterizedType()).getActualTypeArguments()[0]);
        } else {
            Class anyContentClass = getClassFromAnyContent(anyContent);
            if (anyContentClass != null) {
                return getObject(anyContent, anyContentClass);
            } else {
                return getObject(anyContent, pClass);
            }
        }
    }

    /**
     * <p>
     * Decode AnyContent of type <strong>object</strong> as the given class.
     * </p>
     * <p>
     * Preleminary checks are assumed to be done. The method will instantiate the object, and for every items in the anyContent, find a matching field
     * by name, get its class, call {@link #getAnyObjectForField(AnyContent, Field)} to recursivly transform the item and set the field value.
     * </p>
     *
     * @param anyContent AnyContent to decode as object.
     * @param tClass     Targeted object class
     * @param <T>        targeted type of the object
     * @return an instance of the given class fill with the values of the AnyContent element or null if the anyContent does not have any items.
     * @throws MappingException If the AnyContent cannot be transformed as object or if an item does not match any fields in the given class.
     */
    private <T> T decodeObject(AnyContent anyContent, Class<T> tClass) throws MappingException {

        if (anyContent.getItem() != null && !anyContent.getItem().isEmpty()) {
            // ! If a constructor with parameters is used, the items used to feed the parameters are removed from anyContent !
            Class<T> anyContentClass = getClassFromAnyContent(anyContent);

            T object = instantiateObject(anyContent, anyContentClass != null ? anyContentClass : tClass);
            Map<String, Field> fieldsMap = getAllFields(anyContentClass != null ? anyContentClass : tClass);
            for (AnyContent anyContentItem : anyContent.getItem()) {
                setItemValueToField(anyContentClass != null ? anyContentClass : tClass, object, fieldsMap, anyContentItem);
            }
            return object;
        } else {
            return null;
        }
    }

    /**
     * <p>Decode anyContent of type <strong>list</strong> as {@link List}.</p>
     * <p>
     * Preleminary checks are assumed to be done. The method will instantiate the list, and for every items in the anyContent, call {@link
     * #getObject(AnyContent, Class)} to recursivly transform the item.
     * </p>
     *
     * <p>Does not support list of collections</p>
     *
     * @param anyContent   AnyContent to decode as list.
     * @param listClass    Targeted list class
     * @param elementClass Targeted element class
     * @param <L>          targeted type of the list
     * @param <E>          targeted type of the element
     * @return an instance of the given list (or an {@link ArrayList}) filled with the items of the AnyContent element or null if anyContent does not
     * have any items.
     * @throws MappingException If the given AnyContent or one of its items cannot be transformed.
     */
    private <L extends List<?>, E> L decodeList(AnyContent anyContent, Class<L> listClass, Class<E> elementClass) throws MappingException {
        if (anyContent.getItem() != null && !anyContent.getItem().isEmpty()) {
            List<E> objectList = instantiateList(listClass);
            for (AnyContent item : anyContent.getItem()) {
                objectList.add(getObject(item, elementClass));
            }
            return (L) objectList;
        } else {
            return null;
        }
    }

    /**
     * <p>Decode anyContent of type <strong>string</strong> as {@link Enum}.</p>
     *
     * @param anyContent AnyContent to decode as enum.
     * @param tClass     Targeted enumeration class
     * @param <T>        targeted type of the enum
     * @return an instance of the given enumeration according to the AnyContent value or null it the anyContent does not have a value.
     */
    private <T, E extends Enum<E>> T decodeEnum(AnyContent anyContent, Class<T> tClass) {
        if (anyContent.getValue() != null) {
            return (T) Enum.valueOf((Class<E>) tClass, anyContent.getValue());
        } else {
            return null;
        }
    }

    /**
     * <p>Decode anyContent as simple type. Simple types are Java Object values that are directly map to GITB AnyContent values.</p>
     *
     * <p>The methode will dispatch according to the GITB Type : number, boolean or string</p>
     *
     * @param anyContent AnyContent to decode as simpe type.
     * @param tClass     Targeted class
     * @param <T>        Targeted type of the object
     * @return an instance of the given class filled with the AnyContent decoded value or null it the anyContent does not have a value.
     * @throws MappingException If the given AnyContent cannot be decoded as the given class.
     */
    private <T> T decodeTypeSimple(AnyContent anyContent, Class<T> tClass) throws MappingException {
        if (anyContent.getValue() != null) {
            switch (GITBAnyContentType.getFromAnyContentType(anyContent.getType())) {
                case NUMBER:
                    return decodeNumber(anyContent, tClass);
                case BOOLEAN:
                    return decodeBoolean(anyContent);
                case STRING:
                    return decodeString(anyContent);
                default:
                    // Not supposed to happen, just here for Sonar.
                    throw new UnsupportedOperationException(
                            "No GITB Types other than string, number and boolean are supported on the Simple Type Mapping Operation");
            }
        } else {
            return null;
        }
    }

    /**
     * <p>Decode anyContent of type number according to the given class.</p>
     *
     * <p>Supported classes are:
     * <ul>
     * <li>int, {@link Integer}</li>
     * <li>long, {@link Long}</li>
     * <li>float, {@link Float}</li>
     * <li>double, {@link Double}</li>
     * </ul></p>
     *
     * @param anyContent AnyContent to decode as number type.
     * @param tClass     Targeted class.
     * @param <T>        Targeted class
     * @return a decoded number (integer or floating point)
     * @throws MappingException              if the anyContent value cannot be parsed according to the given class.
     * @throws UnsupportedOperationException if the given class is not supported.
     */
    private <T> T decodeNumber(AnyContent anyContent, Class<T> tClass) throws MappingException {
        try {
            if (tClass.equals(Integer.class) || tClass.equals(int.class)) {
                return (T) Integer.valueOf(anyContent.getValue());
            } else if (tClass.equals(Long.class) || tClass.equals(long.class)) {
                return (T) Long.valueOf(anyContent.getValue());
            } else if (tClass.equals(Float.class) || tClass.equals(float.class)) {
                return (T) Float.valueOf(anyContent.getValue());
            } else if (tClass.equals(Double.class) || tClass.equals(double.class)) {
                return (T) Double.valueOf(anyContent.getValue());
            } else {
                throw new UnsupportedOperationException(String.format("Unsupported target class %s to decode GITB number",
                        tClass.getCanonicalName()));
            }
        } catch (NumberFormatException e) {
            throw new MappingException(
                    String.format("Wrong number value for GITB AnyContent [name=%s, type=%s], cannot be decoded as %s", anyContent.getName(),
                            anyContent.getType(), tClass.getCanonicalName()), e);
        }
    }

    /**
     * <p>Decode anyContent of type <strong>boolean</strong> as Boolean.</p>
     * <p>Supported classes are:
     * <ul>
     * <li>boolean, {@link Boolean}</li>
     * </ul>
     *
     * @param anyContent AnyContent to decode as boolean
     * @param <T>        Targeted class.
     * @return a boolean
     * @throws MappingException if the value of the anyContent is neither <strong>true</strong>, neither <strong>false</strong>.
     */
    private <T> T decodeBoolean(AnyContent anyContent) throws MappingException {
        if (anyContent.getValue().equals("true")) {
            return (T) Boolean.TRUE;
        } else if (anyContent.getValue().equals("false")) {
            return (T) Boolean.FALSE;
        } else {
            throw new MappingException(
                    String.format("Wrong boolean value for GITB AnyContent named %s , only 'true' or 'false' values are allowed",
                            anyContent.getName()));
        }
    }

    /**
     * Decode anyContent of type <strong>string</strong> as String.
     *
     * @param anyContent AnyContent to decode as string
     * @param <T>        Targeted class.
     * @return The value of anyContent as string
     */
    private <T> T decodeString(AnyContent anyContent) {
        return (T) anyContent.getValue();
    }

    /**
     * Decode anyContent as {@link Date}. The literal format must be {@link MappingConstant#DATE_FORMAT}
     *
     * @param anyContent AnyContent to decode as Date
     * @param <T>        Targeted class.
     * @return The value of anyContent as Date or null it the anyContent does not have a value.
     * @throws MappingException if the date cannot be parsed according to the format.
     */
    private <T> T decodeDate(AnyContent anyContent) throws MappingException {
        if (anyContent.getValue() != null) {
            try {
                return (T) dateFormat.parse(anyContent.getValue());
            } catch (ParseException e) {
                throw new MappingException(
                        String.format("Wrong date value for GITB AnyContent [name=%s, type=%s, value=%s]", anyContent.getName(),
                                anyContent.getType(), anyContent.getValue()), e);
            }
        } else {
            return null;
        }
    }

    /**
     * Verify if the type of an AnyContent is equals to the expected GITB type of a targeted class.
     *
     * @param anyContent             AnyContent to verify
     * @param expectedAnyContentType expected GITB Type obtain from the targeted class for decoding.
     * @return true if the type is equals to the expected type, false otherwise
     * @see GITBAnyContentType#getFromClass(Class)
     */
    private boolean isTypeCorrect(AnyContent anyContent, GITBAnyContentType expectedAnyContentType) {
        return expectedAnyContentType.getGitbType().equals(anyContent.getType());
    }

    /**
     * Verify if embeddingMethod of the AnyContent is correct according to the mapping operation. If AnyContent is a complex object (ie it have child
     * items), then the embeddingMethod is not verified.
     *
     * @param anyContent       GITB AnyContent element to verify
     * @param mappingOperation Detected mapping operation according to the requested output type.
     * @return true if the embeddingMethod declaration is interpretable by the Mapper, false otherwise
     * @see MappingOperation#getFromClass(Class)
     */
    private boolean isEmbeddingMethodCorrect(AnyContent anyContent, MappingOperation mappingOperation) {
        return anyContent.getValue() == null || anyContent.getEmbeddingMethod().equals(mappingOperation.getEmbeddingMethod());
    }

    /**
     * <p>Create and instance of the given class.</p>
     * <p>It will use constructors that have either no parameters or parameters that have names that match items
     * of the given anyContent. If the class is instantiated using a constructor with parameters, the method will remove from the given AnyContent all
     * items that have been decoded and used as parameters.
     * </p>
     *
     * @param anyContent anyContent object containing items that may be used in the constructor to instantiate the object.
     * @param tClass     class to instantiate
     * @param <T>        class to instantiate
     * @return an object instance of the given class. The object may be initialized with items of the AnyContent that were required in the
     * constructor,
     * if so, those item are remove from the AnyContent.
     * @throws MappingException if the call to the constructor fails or if no constructor is suitable with the given AnyContent.
     */
    private <T> T instantiateObject(AnyContent anyContent, Class<T> tClass) throws MappingException {
        Constructor<T>[] constructors = (Constructor<T>[]) tClass.getDeclaredConstructors();
        sortConstructorByNumberOfParametersAscending(constructors);
        for (Constructor<T> constructor : constructors) {
            Parameter[] parameters = constructor.getParameters();
            Map<Parameter, AnyContent> foundParameters = getMatchingParamItemMap(parameters, anyContent);
            if (isParameterAllFound(parameters, foundParameters)) {
                List<Object> initArgs = new ArrayList<>();
                for (Parameter parameter : parameters) {
                    initArgs
                            .add(getAnyObjectForConstructorArgument(foundParameters.get(parameter), parameter));
                    anyContent.getItem().remove(foundParameters.get(parameter));
                }
                try {
                    constructor.setAccessible(true);
                    return constructor.newInstance(initArgs.toArray());
                } catch (IllegalArgumentException | IllegalAccessException | InvocationTargetException | InstantiationException e) {
                    throw new MappingException(
                            String.format("Unable to instanciate class %s for decoding GITB AnyContent [name=%s, type=%s]", tClass.getCanonicalName(),
                                    anyContent.getName(), anyContent.getType()), e);
                }
            }
        }
        throw new MappingException(
                String.format("Unable to instanciate class %s for decoding GITB AnyContent [name=%s, type=%s], cannot find any suitable constructor",
                        tClass.getCanonicalName(), anyContent.getName(), anyContent.getType()));
    }

    /**
     * Instantiate the given {@link List} implementation. If the given List is an interface or not instantiable, the method will then instantiate an
     * {@link ArrayList}.
     *
     * @param listClass list class to instantiate.
     * @param <L>       targeted list type
     * @param <E>       Element type of the list.
     * @return a list instance of the given list class or an {@link ArrayList} if the given list class was not instantiable.
     */
    private <L extends List<?>, E> List<E> instantiateList(Class<L> listClass) {
        try {
            return (List<E>) listClass.getDeclaredConstructor().newInstance();
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            return new ArrayList<>();
        }
    }

    /**
     * Sort constructor array by parameter count ascending.
     *
     * @param constructors Array of constructors to sort
     * @param <T>          class of the targeted object to construct
     */
    private <T> void sortConstructorByNumberOfParametersAscending(Constructor<T>[] constructors) {
        Arrays.sort(constructors, Comparator.comparingInt(Constructor::getParameterCount));
    }

    /**
     * Get all fields of a class, including those inherited from superclasses.
     *
     * @param tClass the class to look for fields.
     * @param <T>    The type of the class.
     * @return A map of fields, with the name of the field as key and the field as value.
     */
    private <T> Map<String, Field> getAllFields(Class<T> tClass) {
        Map<String, Field> fieldsMap = new HashMap<>();
        Class<? super T> currentClass = tClass;
        while (currentClass != null) {
            for (Field field : currentClass.getDeclaredFields())
                fieldsMap.put(field.getName(), field);
            currentClass = currentClass.getSuperclass();
        }
        return fieldsMap;
    }

    /**
     * Get all constructor parameters that have a name that match with an item of the given anyContent. To be used with {@link
     * #isParameterAllFound(Parameter[], Map)} to determine if a constructor can be used to instantiate an object.
     *
     * @param parameters parameters to look for.
     * @param anyContent anyContent to look at.
     * @return a map of all parameters that have been matching with an anyContent item.
     */
    private Map<Parameter, AnyContent> getMatchingParamItemMap(Parameter[] parameters, AnyContent anyContent) throws MappingException {
        Map<Parameter, AnyContent> foundParameters = new HashMap<>();
        for (Parameter parameter : parameters) {
            for (AnyContent anyContentItem : anyContent.getItem()) {
                if (canBeAssignedToFieldOrParameter(anyContentItem, parameter.getName(), parameter.getType())) {
                    foundParameters.put(parameter, anyContentItem);
                }
            }
        }
        return foundParameters;
    }

    /**
     * Checks if an {@link AnyContent} object can be assigned to a Field or a Constructor parameter.
     * It will check if the {@link AnyContent} item defines a concretion of defined type for the Field/Parameter.
     * If it is indeed a sub-class, will return true, else will return false.
     *
     * @param anyContentItem {@link AnyContent} to check.
     * @param assignedName   Name of the Field/Parameter where we want to map the AnyContent
     * @param assignedClass  Class of the Field/Parameter where we want to map the AnyContent
     * @return wheter or not the {@link AnyContent} can be mapped
     * @throws MappingException if the class defined by the {@link AnyContent} is not found in the mapping context
     */
    private boolean canBeAssignedToFieldOrParameter(AnyContent anyContentItem, String assignedName, Class assignedClass) throws MappingException {
        if (anyContentItem != null) {
            if (anyContentItem.getName() != null && anyContentItem.getName().startsWith(assignedName)) {
                Class anyContentClass = getClassFromAnyContent(anyContentItem);
                if (anyContentClass != null) {
                    return assignedClass.isAssignableFrom(anyContentClass);
                } else {
                    return true;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Get the class defined by an {@link AnyContent}. If none is defined, return null;
     *
     * @param anyContent from which to extract the class
     * @return the class defined by the {@link AnyContent}
     * @throws MappingException if the defined class cannot be found
     */
    private Class getClassFromAnyContent(AnyContent anyContent) throws MappingException {
        if (anyContent.getName() != null && anyContent.getName().contains(":")) {
            String anyContentClassCanonicalName = anyContent.getName().substring(anyContent.getName().indexOf(':') + 1);
            if (anyContentClassCanonicalName != null && !anyContentClassCanonicalName.isEmpty()) {
                try {
                    return Class.forName(anyContentClassCanonicalName);
                } catch (ClassNotFoundException e) {
                    throw new MappingException(String.format("Cannot find class with Canonical Name %s for mapping !",
                            anyContentClassCanonicalName), e);
                }
            }
        }
        return null;
    }


    /**
     * Extract item value from {@link AnyContent} and set it to corresponding field if exists.
     *
     * @param tClass         class of the object to map
     * @param object         instance of the object
     * @param fieldsMap      map containing all fields from the object
     * @param anyContentItem item to map
     * @param <T>            class of the targeted object to map to
     * @throws MappingException
     */
    private <T> void setItemValueToField(Class<T> tClass, T object, Map<String, Field> fieldsMap, AnyContent anyContentItem) throws MappingException {
        try {
            Field matchingField = fieldsMap.get(getParamNameFromAnyContent(anyContentItem.getName()));
            if (matchingField != null) {
                matchingField.setAccessible(true);
                Object fieldObject = getAnyObjectForField(anyContentItem, matchingField);
                if (fieldObject != null) {
                    matchingField.set(object, fieldObject);
                }
            } else {
                throw new MappingException(String.format("Unable to find field named %s in class %s and its superclasses", anyContentItem.getName(),
                        tClass.getCanonicalName()));
            }
        } catch (IllegalAccessException e) {
            throw new MappingException(getIllegalAccessFieldExceptionMessage(anyContentItem, tClass, e), e);
        }
    }

    /**
     * Get parameter name from {@link AnyContent} name by removing the class specified at its end if exists
     *
     * @param anyContentName name of the {@link AnyContent} element
     * @return Name without class
     */
    private String getParamNameFromAnyContent(String anyContentName) {
        if (anyContentName != null && anyContentName.contains(":")) {
            return anyContentName.substring(0, anyContentName.indexOf(':'));
        }
        return anyContentName;
    }

    /**
     * Check if all parameters of a constructor are present in the map of parameters that match a name of an item. If true, it means the constructor
     * can be used to instantiate an object.
     *
     * @param parameters      Complete list of parameters of the constructor
     * @param foundParameters map of parameters that are matching an item of the anyContent.
     * @return true if all paramters of a constructor could be instantiated from the anycontent, false otherwise.
     * @see #getMatchingParamItemMap(Parameter[], AnyContent) ;
     */
    private boolean isParameterAllFound(Parameter[] parameters, Map<Parameter, AnyContent> foundParameters) {
        return foundParameters.keySet().containsAll(Arrays.asList(parameters));
    }


    /**
     * Get exception message when unable to set a value in a field
     *
     * @param anyContentItem AnyContent from which the value is read
     * @param clazz          targeted class where the field cannot be set
     * @param e              IllegalAccessException that have bee raised
     * @return A String message to describe the MappingException
     */
    private String getIllegalAccessFieldExceptionMessage(AnyContent anyContentItem, Class<?> clazz, IllegalAccessException e) {
        return String.format("Unable to access field named %s in class %s. %s", anyContentItem.getName(),
                clazz.getCanonicalName(), e.getMessage().contains("static") ? "Static fields cannot be set" : null);
    }
}
