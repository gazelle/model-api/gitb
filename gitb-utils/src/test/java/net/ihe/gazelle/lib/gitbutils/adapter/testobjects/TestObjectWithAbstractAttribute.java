package net.ihe.gazelle.lib.gitbutils.adapter.testobjects;

public class TestObjectWithAbstractAttribute {

    public TestAttributeObject attributeObject;

    /**
     * Default constructor for the class
     * @param attributeObject initial value of the attributeObject property
     */
    public TestObjectWithAbstractAttribute(TestAttributeObject attributeObject){
        this.attributeObject = attributeObject;
    }

    /**
     * Getter for the attributeObject property
     * @return the value of th property
     */
    public TestAttributeObject getAttributeObject() {
        return attributeObject;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        TestObjectWithAbstractAttribute that = (TestObjectWithAbstractAttribute) o;

        return attributeObject != null ? attributeObject.equals(that.attributeObject) : that.attributeObject == null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return attributeObject != null ? attributeObject.hashCode() : 0;
    }
}
