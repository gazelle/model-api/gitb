package net.ihe.gazelle.lib.gitbutils.adapter;

import com.gitb.core.AnyContent;
import com.gitb.core.ValueEmbeddingEnumeration;
import net.ihe.gazelle.lib.annotations.Covers;
import net.ihe.gazelle.lib.annotations.Package;
import net.ihe.gazelle.lib.gitbutils.adapter.testobjects.TestEnum;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.stream.Stream;

/**
 * Test Mapping from AnyContent to objects for simple elements (String, numbers, boolean, dates, enumeration)
 *
 * @author ceoche, nbaillet
 */
@Covers(requirements = {"GITB-6", "GITB-31"})
public class MapperAnyContentToPrimitiveObjectTest {

   private MapperAnyContentToObject mapper = new MapperAnyContentToObject();
   private SimpleDateFormat dateFormat = new SimpleDateFormat(MappingConstant.DATE_FORMAT);

   /**
    * Verify the mapper is able to transform {@link AnyContent} as simple objects.
    * <p>
    * This test is provided with a stream of arguments. It will create an AnyContent instance, call the mapper and compare with the expected value.
    *
    * @param tClass the targeted output class
    * @param type   GITB type of the AnyContent
    * @param value  value of the AnyContent
    * @param <T>    output class
    *
    * @throws MappingException in case of mapping error
    * @throws ParseException   in case of date convertion error
    */
   @ParameterizedTest
   @MethodSource("nominalAnyContentMappingProvider")
   public <T> void testMapAnyContentToPrimitive(Class<T> tClass, String type, String value) throws MappingException, ParseException {
      AnyContent anyContentPositive = createAnyContent("myVar", type, value, ValueEmbeddingEnumeration.STRING);

      T objectValue = mapper.getObject(anyContentPositive, tClass);

      assertPrimitiveEquals(tClass, value, objectValue,
            String.format("The mapper must be able to decode %s values and associate to Java %s", type, tClass.getName()));
   }

   /**
    * Provide a Stream of arguments for nominal simple case mapping (Targeted class, GITB type and value)
    *
    * @return a stream of arguments
    */
   @Covers(requirements = {"GITB-42", "GITB-44", "GITB-45", "GITB-46", "GITB-47", "GITB-51", "GITB-53", "GITB-93", "GITB-32", "GITB-92"})
   @Package
   static Stream<Arguments> nominalAnyContentMappingProvider() {
      return Stream.of(
            Arguments.arguments(Integer.class, GITBAnyContentType.NUMBER.getGitbType(), "155"),
            Arguments.arguments(int.class, GITBAnyContentType.NUMBER.getGitbType(), "-34001"),
            Arguments.arguments(Long.class, GITBAnyContentType.NUMBER.getGitbType(), "2147493649"),
            Arguments.arguments(long.class, GITBAnyContentType.NUMBER.getGitbType(), "-3145483649"),
            Arguments.arguments(Float.class, GITBAnyContentType.NUMBER.getGitbType(), "214.25545"),
            Arguments.arguments(float.class, GITBAnyContentType.NUMBER.getGitbType(), "-0.0000000000000000000568845585855"),
            Arguments.arguments(Double.class, GITBAnyContentType.NUMBER.getGitbType(), "28898989894514.25545"),
            Arguments.arguments(double.class, GITBAnyContentType.NUMBER.getGitbType(), "-28898989894514.25545"),
            Arguments.arguments(Boolean.class, GITBAnyContentType.BOOLEAN.getGitbType(), "true"),
            Arguments.arguments(boolean.class, GITBAnyContentType.BOOLEAN.getGitbType(), "false"),
            Arguments.arguments(String.class, GITBAnyContentType.STRING.getGitbType(), "The value of my string"),
            Arguments.arguments(Date.class, GITBAnyContentType.STRING.getGitbType(), "2020-03-05T08:25:18.000+0100")
      );
   }

   /**
    * Verify Mapper is able to detect wrong embedding method. This is using the {@link #nominalAnyContentMappingProvider()}, but modifying the type.
    *
    * @param tClass the targeted output class * @param type   GITB type of the AnyContent * @param value  value of the AnyContent * @param <T> output
    *               class
    */
   @ParameterizedTest
   @MethodSource("nominalAnyContentMappingProvider")
   @Covers(requirements = {"GITB-52", "GITB-54", "GITB-69", "GITB-70"})
   public <T> void testAnyContentPrimitiveEmbeddingMethodError(Class<T> tClass, String type, String value) {
      AnyContent anyContent = createAnyContent("myVar", type, value, ValueEmbeddingEnumeration.BASE_64);

      Assertions.assertThrows(MappingException.class, () -> mapper.getObject(anyContent, tClass),
            "The mapper must raise an error if the embeddingMethod is not STRING");
   }

   /**
    * <p>Verify Mapper is able to detect wrong Values. I.e. values that are not decodable in the requested type</p>
    * <p>This test is provided with a stream of arguments. It will create an AnyContent instance, call the mapper and wait for the exception.</p>
    *
    * @param tClass the targeted output class
    * @param type   GITB type of the AnyContent
    * @param value  value of the AnyContent
    * @param <T>    output class
    */
   @ParameterizedTest
   @MethodSource("wrongValueAnyContentMappingProvider")
   public <T> void testAnyContentPrimitiveWrongValueError(Class<T> tClass, String type, String value) {
      AnyContent anyContent = createAnyContent("myVar", type, value, ValueEmbeddingEnumeration.STRING);

      Assertions.assertThrows(MappingException.class, () -> mapper.getObject(anyContent, tClass),
            String.format("The mapper must raise an error if it can not parse the %s value as %s", type, tClass.getName()));
   }

   /**
    * Provide a Stream of arguments for wrong values (not decodable in the expected class) of simple case mapping (Targeted class, GITB type and
    * value)
    *
    * @return a stream of arguments
    */
   @Covers(requirements = {"GITB-44", "GITB-45", "GITB-46", "GITB-54", "GITB-69", "GITB-93", "GITB-32", "GITB-92"})
   @Package
   static Stream<Arguments> wrongValueAnyContentMappingProvider() {
      return Stream.of(
            Arguments.arguments(Integer.class, GITBAnyContentType.NUMBER.getGitbType(), "12.45"),
            Arguments.arguments(int.class, GITBAnyContentType.NUMBER.getGitbType(), "Riri"),
            Arguments.arguments(Long.class, GITBAnyContentType.NUMBER.getGitbType(), "2147483649.456"),
            Arguments.arguments(long.class, GITBAnyContentType.NUMBER.getGitbType(), "fifi"),
            Arguments.arguments(Float.class, GITBAnyContentType.NUMBER.getGitbType(), "loulou"),
            Arguments.arguments(float.class, GITBAnyContentType.NUMBER.getGitbType(), "picsou"),
            Arguments.arguments(Double.class, GITBAnyContentType.NUMBER.getGitbType(), "donald"),
            Arguments.arguments(double.class, GITBAnyContentType.NUMBER.getGitbType(), "infinity"),
            Arguments.arguments(Boolean.class, GITBAnyContentType.BOOLEAN.getGitbType(), "not a boolean"),
            Arguments.arguments(boolean.class, GITBAnyContentType.BOOLEAN.getGitbType(), "9898"),
            Arguments.arguments(Date.class, GITBAnyContentType.STRING.getGitbType(), "2020-03-05")
      );
   }

   /**
    * <p>Verify Mapper is able to detect wrong GITB Type.</p>
    * <p>This test is provided with a stream of arguments. It will create an AnyContent instance, call the mapper and wait for the exception.</p>
    *
    * @param tClass the targeted output class
    * @param type   GITB type of the AnyContent
    * @param value  value of the AnyContent
    * @param <T>    output class
    */
   @ParameterizedTest
   @MethodSource("wrongTypeAnyContentMappingProvider")
   @Covers(requirements = {"GITB-39", "GITB-43", "GITB-48", "GITB-50"})
   public <T> void testAnyContentPrimitiveWrongTypeError(Class<T> tClass, String type, String value) {
      AnyContent anyContent = createAnyContent("myVar", type, value, ValueEmbeddingEnumeration.STRING);

      Assertions.assertThrows(MappingException.class, () -> mapper.getObject(anyContent, tClass),
            String.format("The Mapper must raise an error if the requested type %s cannot be obtained from the AnyContent type %s", tClass.getName(),
                  type));
   }

   /**
    * Provide a Stream of arguments for wrong GITB types (Targeted class, GITB type and value)
    *
    * @return a stream of arguments
    */
   @Package
   static Stream<Arguments> wrongTypeAnyContentMappingProvider() {
      return Stream.of(
            Arguments.arguments(Integer.class, "boolean", "155"),
            Arguments.arguments(int.class, "string", "-34001"),
            Arguments.arguments(Long.class, "binary", "2147493649"),
            Arguments.arguments(long.class, "object", "-3145483649"),
            Arguments.arguments(Float.class, "list", "214.25545"),
            Arguments.arguments(float.class, "map", "-0.0000000000000000000568845585855"),
            Arguments.arguments(Double.class, "toto", "28898989894514.25545"),
            Arguments.arguments(double.class, "string", "-28898989894514.25545"),
            Arguments.arguments(Boolean.class, "string", "true"),
            Arguments.arguments(boolean.class, "number", "false"),
            Arguments.arguments(String.class, "number", "The value of my string"),
            Arguments.arguments(Date.class, "boolean", "2020-03-05T08:25:18.000+0100")
      );
   }

   /**
    * <p>Verify Mapper is able to detect overflow parsing of integers (int and long)</p>
    *
    * @param tClass the targeted output class
    * @param value  value of the AnyContent
    * @param <T>    output class
    */
   @ParameterizedTest
   @MethodSource("overflowValueAnyContentMappingProvider")
   public <T> void testAnyContentNumberOverFlowError(Class<T> tClass, String value) {
      AnyContent anyContent = createAnyContent("myInteger", GITBAnyContentType.NUMBER.getGitbType(), value, ValueEmbeddingEnumeration.STRING);

      Assertions.assertThrows(MappingException.class, () -> mapper.getObject(anyContent, tClass),
            "The mapper must raise an error if the integer to be parsed is out of the Java-int/long range (overflow).");
   }

   /**
    * Provide a Stream of arguments for integers that can be overflooded (Targeted class, GITB type and value). Float and double numbers can not be
    * tested for overflow (rounded value and infinity are supported)
    *
    * @return a stream of arguments
    */
   @Covers(requirements = {"GITB-44", "GITB-45"})
   @Package
   static Stream<Arguments> overflowValueAnyContentMappingProvider() {
      return Stream.of(
            Arguments.arguments(Integer.class, "2147483649"),
            Arguments.arguments(int.class, "-3147483649"),
            Arguments.arguments(Long.class, "92233720370000000000"),
            Arguments.arguments(long.class, "-862233720370000000000")
      );
   }

   /**
    * Verify the mapper is able to transform AnyContent to an Enumeration
    *
    * @throws MappingException in case of mapping error
    */
   @Test
   @Covers(requirements = {"GITB-57"})
   public void testMapAnyContentToEnum() throws MappingException {
      AnyContent anyContentPositive = createAnyContent("myEnum", GITBAnyContentType.STRING.getGitbType(), TestEnum.VALUE1.name(),
            ValueEmbeddingEnumeration.STRING);

      TestEnum objectEnum = mapper.getObject(anyContentPositive, TestEnum.class);

      Assertions.assertEquals(TestEnum.VALUE1, objectEnum,
            String.format("The mapper must be able to decode string values and associate to Java enumeration"));
   }

   /**
    * Verify the mapper is able to detect that the embedding method is wrong for an Enumeration
    */
   @Test
   @Covers(requirements = {"GITB-58"})
   public void testAnyContentEnumEmbeddingMethodError() {
      AnyContent anyContent = createAnyContent("myEnum", GITBAnyContentType.STRING.getGitbType(), TestEnum.VALUE1.name(),
            ValueEmbeddingEnumeration.URI);

      Assertions.assertThrows(MappingException.class, () -> mapper.getObject(anyContent, TestEnum.class),
            "The mapper must raise an error if the embeddingMethod is not STRING");
   }

   /**
    * Verify the mapper raise an error if the given value is not part of the targeted enumeration.
    */
   @Test
   @Covers(requirements = {"GITB-58"})
   public void testAnyContentEnumWrongValueError() {
      AnyContent anyContent = createAnyContent("myEnum", GITBAnyContentType.STRING.getGitbType(), "NOVALUE", ValueEmbeddingEnumeration.URI);

      Assertions.assertThrows(MappingException.class, () -> mapper.getObject(anyContent, TestEnum.class),
            "The mapper must raise an error if the value is not decodeable for the given enumeration");
   }

   /**
    * Super assertEquals for any primitive type supported for the mapper (String, date, numbers, boolean)
    *
    * @param tClass        Class type of the transformed object
    * @param expectedValue expected value in String
    * @param actualValue   actuale value in the given class type
    * @param message       assertion message in case of test error
    * @param <T>           class type
    *
    * @throws ParseException in case of date parsing error.
    */
   private <T> void assertPrimitiveEquals(Class<T> tClass, String expectedValue, T actualValue, String message) throws ParseException {
      if (tClass.equals(String.class)) {
         Assertions.assertEquals(expectedValue, (String) actualValue, message);
      } else if (tClass.equals(Date.class)) {
         Assertions.assertEquals(dateFormat.parse(expectedValue), (Date) actualValue, message);
      } else if (tClass.equals(Integer.class) || tClass.equals(int.class)) {
         Assertions.assertEquals(Integer.valueOf(expectedValue), (Integer) actualValue, message);
      } else if (tClass.equals(Long.class) || tClass.equals(long.class)) {
         Assertions.assertEquals(Long.valueOf(expectedValue), (Long) actualValue, message);
      } else if (tClass.equals(Float.class) || tClass.equals(float.class)) {
         Assertions.assertEquals(Float.valueOf(expectedValue), (Float) actualValue, message);
      } else if (tClass.equals(Double.class) || tClass.equals(double.class)) {
         Assertions.assertEquals(Double.valueOf(expectedValue), (Double) actualValue, message);
      } else if (tClass.equals(Boolean.class) || tClass.equals(boolean.class)) {
         Assertions.assertEquals(Boolean.valueOf(expectedValue), (Boolean) actualValue, message);
      } else {
         Assertions.fail();
      }
   }

   /**
    * Shortcut method to create an {@link AnyContent} element with value.
    *
    * @param name            name of the AnyContent
    * @param type            type of the AnyContent
    * @param value           value of the AnyContent
    * @param embeddingMethod embedding method of the AnyContent
    *
    * @return an instance of AnyContent set with given parameters.
    */
   private AnyContent createAnyContent(String name, String type, String value, ValueEmbeddingEnumeration embeddingMethod) {
      AnyContent anyContent = new AnyContent();
      anyContent.setName(name);
      anyContent.setType(type);
      anyContent.setValue(value);
      anyContent.setEmbeddingMethod(embeddingMethod);
      return anyContent;
   }

}
