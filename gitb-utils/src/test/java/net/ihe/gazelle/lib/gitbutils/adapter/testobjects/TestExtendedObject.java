package net.ihe.gazelle.lib.gitbutils.adapter.testobjects;

/**
 * This object extend another with several attributes. This object is used to test the mapper is able to map fields from superclass.
 */
public class TestExtendedObject extends TestAttributeObject {

    private String testString;

    /**
     * Constructor with id.
     * @param id     value of the id property to set
     */
    public TestExtendedObject(int id) {
        super(id);
    }

    /**
     * Setter for the testString property
     * @param testString  value of the testString property to set
     */
    public void setTestString(String testString){
        this.testString = testString;
    }

    /**
     * Getter for testString property.
     * @return the value of the property.
     */
    public String getTestString() {
        return testString;
    }
}
