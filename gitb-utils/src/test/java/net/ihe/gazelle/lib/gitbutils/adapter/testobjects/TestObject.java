package net.ihe.gazelle.lib.gitbutils.adapter.testobjects;

import net.ihe.gazelle.lib.gitbutils.adapter.GITBAnyContentType;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * Test object with multiple types of fields. Used to test the mapper is correctly handling object fields.
 */
public class TestObject {

    private String testString;
    private Boolean testBoolean;
    private Integer testInt;
    private List<String> testList = new ArrayList<>();
    private Date testDate;
    private GITBAnyContentType testEnum;
    private TestAttributeObject attributeObject;

    /**
     * Complete constructor for the object.
     * @param testString         value of the testString property to set
     * @param testBoolean        value of the testBoolean property to set
     * @param testInt            value of the testInt property to set
     * @param testList           value of the testList property to set
     * @param testDate           value of the testDate property to set
     * @param testEnum           value of the testEnum property to set
     * @param attributeObject    value of the attributeObject property to set
     */
    public TestObject(String testString, Boolean testBoolean, Integer testInt, List<String> testList, Date testDate, GITBAnyContentType testEnum,
                      TestAttributeObject attributeObject) {
        this.testString = testString;
        this.testBoolean = testBoolean;
        this.testInt = testInt;
        this.testList = testList;
        this.testDate = testDate;
        this.testEnum = testEnum;
        this.attributeObject = attributeObject;
    }

    /**
     * Getter for the testString property
     * @return the value of the property
     */
    public String getTestString() {
        return testString;
    }

    /**
     * Getter for the testBoolean property
     * @return the value of the property
     */
    public Boolean getTestBoolean() {
        return testBoolean;
    }

    /**
     * Getter for the testInt property
     * @return the value of the property
     */
    public int getTestInt() {
        return testInt;
    }

    /**
     * Getter for the testList property
     * @return the value of the property
     */
    public List<String> getTestList() {
        return testList;
    }

    /**
     * Getter for the testDate property
     * @return the value of the property
     */
    public Date getTestDate() {
        return testDate;
    }

    /**
     * Getter for the testEnum property
     * @return the value of the property
     */
    public GITBAnyContentType getTestEnum() {
        return testEnum;
    }

    /**
     * Getter for the attributeObject property
     * @return the value of the property
     */
    public TestAttributeObject getAttributeObject() {
        return attributeObject;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TestObject that = (TestObject) o;
        return Objects.equals(testString, that.testString) &&
              Objects.equals(testBoolean, that.testBoolean) &&
              Objects.equals(testInt, that.testInt) &&
              Objects.equals(testList, that.testList) &&
              Objects.equals(testDate, that.testDate) &&
              testEnum == that.testEnum &&
              Objects.equals(attributeObject, that.attributeObject);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hash(testString, testBoolean, testInt, testList, testDate, testEnum, attributeObject);
    }
}
