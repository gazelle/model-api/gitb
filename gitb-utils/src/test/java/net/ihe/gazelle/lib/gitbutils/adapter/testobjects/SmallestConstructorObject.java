package net.ihe.gazelle.lib.gitbutils.adapter.testobjects;


import net.ihe.gazelle.lib.gitbutils.adapter.GITBAnyContentType;
import org.junit.jupiter.api.Assertions;

import java.util.Date;
import java.util.List;

/**
 * Test classes with multiple constructor. It aims to test that the mapper instantiate the object with the smallest constructor possible.
 */
public class SmallestConstructorObject extends TestObject {

   /**
    * Complex constructor for the object.
    * @param testString         value of the testString property to set
    * @param testBoolean        value of the testBoolean property to set
    * @param testInt            value of the testInt property to set
    * @param testList           value of the testList property to set
    * @param testDate           value of the testDate property to set
    * @param testEnum           value of the testEnum property to set
    * @param attributeObject    value of the attributeObject property to set
    */
   public SmallestConstructorObject(String testString, Boolean testBoolean, Integer testInt, List<String> testList, Date testDate,
                                    GITBAnyContentType testEnum,
                                    TestAttributeObject attributeObject) {
      super(testString, testBoolean, testInt, testList, testDate, testEnum, attributeObject);
      Assertions.fail("the mapper must call the smallest constructor available");
   }

   /**
    * Smaller constructor with only one parameter for the testString property.
    * @param testString        value of the testString property to set
    */
   public SmallestConstructorObject(String testString) {
      super(testString, null, null, null, null, null, null);
      Assertions.fail("the mapper must call the smallest constructor available");
   }

   /**
    * Smallest Constructor with no arguments
    */
   public SmallestConstructorObject() {
      super(null, null, null, null, null, null, null);
   }
}
