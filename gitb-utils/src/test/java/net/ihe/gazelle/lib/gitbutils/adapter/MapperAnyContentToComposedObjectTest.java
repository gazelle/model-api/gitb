package net.ihe.gazelle.lib.gitbutils.adapter;

import com.gitb.core.AnyContent;
import com.gitb.core.ValueEmbeddingEnumeration;
import net.ihe.gazelle.lib.annotations.Covers;
import net.ihe.gazelle.lib.annotations.Package;
import net.ihe.gazelle.lib.gitbutils.adapter.testobjects.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Stream;

/**
 * Test Mapping from AnyContent to objects for composes elements (Objects and collections)
 *
 * @author ceoche
 */
@Covers(requirements = {"GITB-6", "GITB-31"})
public class MapperAnyContentToComposedObjectTest {

   private static DateFormat dateFormat = new SimpleDateFormat(MappingConstant.DATE_FORMAT);

   private MapperAnyContentToObject mapper = new MapperAnyContentToObject();

   /**
    * Verify the mapper is able to transform AnyContent to List
    *
    * @param elementClass        list class requested as output
    * @param expectedList        expected Output List instance to compare with (assert equals)
    * @param anyContentValueList list of values as string to build AnyContent items
    * @param <E>                 requested output class
    *
    * @throws MappingException in case of mapping error
    */
   @ParameterizedTest
   @MethodSource("contentListProvider")
   @Covers(requirements = {"GITB-64", "GITB-96"})
   public <E> void testAnyContentToList(Class<E> elementClass, List<E> expectedList, List<String> anyContentValueList) throws MappingException {
      AnyContent listAsAnyContent = createAnyContentContainer("myList", GITBAnyContentType.LIST);
      for (String anyContentValue : anyContentValueList) {
         listAsAnyContent.getItem().add(createAnyContentValue(null, GITBAnyContentType.STRING, ValueEmbeddingEnumeration.STRING, anyContentValue));
      }

      List<String> listAsObject = mapper.getObjectCollection(listAsAnyContent, ArrayList.class, elementClass);

      Assertions.assertEquals(expectedList, listAsObject, "Mapper must be able to decode GITB AnyContent List as java List");
   }

   /**
    * Provide test nominal test parameters for list mapping
    *
    * @return a stream of arguments (Class, expected list, list values as string of the AnyContent
    *
    * @throws ParseException
    */
   @Package
   static Stream<Arguments> contentListProvider() throws ParseException {
      String[] stringValues = {"Value of item1", "Value of item2"};

      final String DATE_1 = "2020-03-05T08:25:18.000+0100";
      final String DATE_2 = "2020-03-05T13:31:42.000+0100";
      Date[] dates = {dateFormat.parse(DATE_1), dateFormat.parse(DATE_2)};
      String[] dateValues = {DATE_1, DATE_2};

      return Stream.of(
            Arguments.arguments(String.class, Arrays.asList(stringValues), Arrays.asList(stringValues)),
            Arguments.arguments(Date.class, Arrays.asList(dates), Arrays.asList(dateValues))
      );
   }

   /**
    * Verify Mapper is able to detect wrong GITB Type for lists. This is using the {@link #contentListProvider()}, but modifying the type.
    *
    * @param elementClass        list class requested as output
    * @param expectedList        expected Output List instance to compare with (assert equals)
    * @param anyContentValueList list of values as string to build AnyContent items
    * @param <E>                 requested output class
    */
   @ParameterizedTest
   @MethodSource("contentListProvider")
   public <E> void testAnyContentListWrongType(Class<E> elementClass, List<E> expectedList, List<String> anyContentValueList) {
      AnyContent listAsAnyContent = createAnyContentContainer("myList", GITBAnyContentType.NUMBER);

      Assertions.assertThrows(MappingException.class, () -> mapper.getObjectCollection(listAsAnyContent, ArrayList.class, elementClass));
   }

   /**
    * Verify Mapper is able to detect wrong GITB Type for list items. This is using the {@link #contentListProvider()}, but modifying the type.
    *
    * @param elementClass        list class requested as output
    * @param expectedList        expected Output List instance to compare with (assert equals)
    * @param anyContentValueList list of values as string to build AnyContent items
    * @param <E>                 requested output class
    */
   @ParameterizedTest
   @MethodSource("contentListProvider")
   @Covers(requirements = {"GITB-65"})
   public <E> void testAnyContentListWrongItemType(Class<E> elementClass, List<E> expectedList, List<String> anyContentValueList) {
      AnyContent listAsAnyContent = createAnyContentContainer("myList", GITBAnyContentType.LIST);
      for (String anyContentValue : anyContentValueList) {
         listAsAnyContent.getItem().add(createAnyContentValue(null, GITBAnyContentType.BOOLEAN, ValueEmbeddingEnumeration.STRING, anyContentValue));
      }

      Assertions.assertThrows(MappingException.class, () -> mapper.getObjectCollection(listAsAnyContent, ArrayList.class, elementClass));
   }

   /**
    * Verify the mapper is able to transform a simple AnyContent as object (object with one string, one boolean and one int)
    *
    * @throws MappingException in case of mapping error
    */
   @Test
   @Covers(requirements = {"GITB-36", "GITB-37", "GITB-59", "GITB-95"})
   public void testAnyContentToSimpleObject() throws MappingException {
      final String TITLE = "A good title for a simple object";
      SimpleTestObject expectedSimpleTestObject = generateSimpleObjectObject(TITLE);
      AnyContent anyContent = generateSimpleObjectAnyContent("mySimpleTestObject", TITLE);

      SimpleTestObject simpleTestObject = mapper.getObject(anyContent, SimpleTestObject.class);

      Assertions.assertEquals(expectedSimpleTestObject, simpleTestObject, "The mapper must be able to decode simple object with default constructor");
   }

   /**
    * Verify the mapper raise an error if an AnyContent cannot be set in a field of the given object.
    */
   @Test
   @Covers(requirements = {"GITB-38", "GITB-95"})
   public void testAnyContentToObjectNoFieldError() {
      final String TITLE = "A good title for a simple object";
      SimpleTestObject expectedSimpleTestObject = generateSimpleObjectObject(TITLE);
      AnyContent anyContent = generateSimpleObjectAnyContent("mySimpleTestObject", TITLE);
      AnyContent anyContentExtra = createAnyContentValue("extra", GITBAnyContentType.STRING, ValueEmbeddingEnumeration.STRING,
            "This anycontent is not defined in the class");
      anyContent.getItem().add(anyContentExtra);

      Assertions.assertThrows(MappingException.class, () ->
            mapper.getObject(anyContent, SimpleTestObject.class), "A mapping error must be raised if an anycontent cannot be feed in any field");
   }

   /**
    * Verify the mapper raise an error if the type of an AnyContent does not correspond to the class of the receiving field.
    */
   @Test
   @Covers(requirements = {"GITB-39", "GITB-95"})
   public void testAnyContentToObjectWrongTypeItemError() {
      final String TITLE = "A good title for a simple object";
      SimpleTestObject expectedSimpleTestObject = generateSimpleObjectObject(TITLE);
      AnyContent anyContent = generateSimpleObjectAnyContent("mySimpleTestObject", TITLE);
      anyContent.getItem().get(2).setType(GITBAnyContentType.LIST.getGitbType());

      Assertions.assertThrows(MappingException.class, () ->
                  mapper.getObject(anyContent, SimpleTestObject.class),
            "A mapping error must be raised if an anycontent cannot decoded according to the field's type");
   }

   /**
    * GITB-40 Verify anyContent without values or items are ignored.
    *
    * @throws MappingException in case of mapping error
    */
   @Test
   @Covers(requirements = {"GITB-40", "GITB-95"})
   public void testAnyContentWithoutValueNorItemsIgnored() throws MappingException {
      TestObject expectedObject = new TestObject("This a string for test", null, null, null, null, null, null);
      AnyContent anyContent = createAnyContentContainer("testObject", GITBAnyContentType.OBJECT);
      anyContent.getItem()
            .add(createAnyContentValue("testString", GITBAnyContentType.STRING, ValueEmbeddingEnumeration.STRING, expectedObject.getTestString()));
      anyContent.getItem().add(createAnyContentValue("testBoolean", GITBAnyContentType.BOOLEAN, ValueEmbeddingEnumeration.STRING, null));
      anyContent.getItem().add(createAnyContentValue("testInt", GITBAnyContentType.NUMBER, ValueEmbeddingEnumeration.STRING, null));
      anyContent.getItem().add(createAnyContentValue("testDate", GITBAnyContentType.STRING, ValueEmbeddingEnumeration.STRING, null));
      anyContent.getItem().add(createAnyContentContainer("testList", GITBAnyContentType.LIST));
      anyContent.getItem().add(createAnyContentValue("testEnum", GITBAnyContentType.STRING, ValueEmbeddingEnumeration.STRING, null));
      anyContent.getItem().add(createAnyContentContainer("attributeObject", GITBAnyContentType.OBJECT));

      TestObject decodedObject = mapper.getObject(anyContent, TestObject.class);

      Assertions.assertEquals(expectedObject, decodedObject, "The mapper must ignore empty anyContent values and items");
   }

   /**
    * GITB-60 Verify smallest constructor matching is used to instantiate an object.
    *
    * @throws MappingException in case of mapping error
    */
   @Test
   @Covers(requirements = {"GITB-60", "GITB-95"})
   public void testSmallestConstructorIsCalled() throws MappingException {
      AnyContent anyContent = createAnyContentContainer("testObject", GITBAnyContentType.OBJECT);
      anyContent.getItem()
            .add(createAnyContentValue("testString", GITBAnyContentType.STRING, ValueEmbeddingEnumeration.STRING, "This a string for test"));

      SmallestConstructorObject decodedObject = mapper.getObject(anyContent, SmallestConstructorObject.class);

      Assertions.assertEquals("This a string for test", decodedObject.getTestString(),
            "The mapper must have construct the object using the constructor without parameters");
   }

   /**
    * GITB-63 Verify an object cannot be instantiated because of a missing argument
    */
   @Test
   @Covers(requirements = {"GITB-63", "GITB-95"})
   public void testMissingConstructorArgument() {
      AnyContent anyContent = createAnyContentContainer("testObject", GITBAnyContentType.OBJECT);
      anyContent.getItem().add(createAnyContentValue("testBoolean", GITBAnyContentType.BOOLEAN, ValueEmbeddingEnumeration.STRING, "true"));
      anyContent.getItem().add(createAnyContentValue("testInt", GITBAnyContentType.NUMBER, ValueEmbeddingEnumeration.STRING, "23"));
      anyContent.getItem()
            .add(createAnyContentValue("testDate", GITBAnyContentType.STRING, ValueEmbeddingEnumeration.STRING, dateFormat.format(new Date())));
      anyContent.getItem().add(createAnyContentContainer("testList", GITBAnyContentType.LIST));
      anyContent.getItem()
            .add(createAnyContentValue("testEnum", GITBAnyContentType.STRING, ValueEmbeddingEnumeration.STRING, GITBAnyContentType.STRING.name()));
      anyContent.getItem().add(createAnyContentContainer("attributeObject", GITBAnyContentType.OBJECT));

      Assertions.assertThrows(MappingException.class, () -> mapper.getObject(anyContent, TestObject.class),
            "The mapper must raise an error if it cannot instantiate an object beacause of a missing argument");
   }

   /**
    * Verify the mapper is able to instantiate an object using a constructor with parameters Verify the mapper supports inheritance
    *
    * @throws MappingException in case of mapping error
    */
   @Test
   @Covers(requirements = {"GITB-62", "GITB-95"})
   public void testAnyContentToObjectWithConstructorAndInheritance() throws MappingException {
      final String TITLE = "A better title for a less simple object";
      final String IDENTIFIER = "object-1";
      TestObjectWithConstructorAndInheritance expectedObjectWithConstructor = new TestObjectWithConstructorAndInheritance(IDENTIFIER);
      expectedObjectWithConstructor.setTitle(TITLE);
      expectedObjectWithConstructor.setActivated(true);
      expectedObjectWithConstructor.setIndex(23);
      AnyContent anyContent = createAnyContentContainer("myTestObjectWithConstructor", GITBAnyContentType.OBJECT);
      anyContent.getItem().add(createAnyContentValue("identifier", GITBAnyContentType.STRING, ValueEmbeddingEnumeration.STRING, IDENTIFIER));
      anyContent.getItem().add(createAnyContentValue("title", GITBAnyContentType.STRING, ValueEmbeddingEnumeration.STRING, TITLE));
      anyContent.getItem().add(createAnyContentValue("activated", GITBAnyContentType.BOOLEAN, ValueEmbeddingEnumeration.STRING, "true"));
      anyContent.getItem().add(createAnyContentValue("index", GITBAnyContentType.NUMBER, ValueEmbeddingEnumeration.STRING, Integer.toString(23)));
      anyContent.getItem().add(createAnyContentValue("updateTime", GITBAnyContentType.STRING, ValueEmbeddingEnumeration.STRING,
            dateFormat.format(expectedObjectWithConstructor.getUpdateTime())));

      TestObjectWithConstructorAndInheritance objectWithConstructor = mapper.getObject(anyContent, TestObjectWithConstructorAndInheritance.class);

      Assertions.assertEquals(expectedObjectWithConstructor, objectWithConstructor,
            "The mapper must be able to decode simple object with complex constructor");
   }

   /**
    * Verify the mapper is able to instantiate an object using a private/package/protected constructor.
    *
    * @throws MappingException in case of mapping error
    */
   @Test
   @Covers(requirements = {"GITB-59", "GITB-95"})
   public void testAnyContentToObjectWithHiddenConstructorNestedListAndObject() throws MappingException {
      final String TITLE = "A good title for a simple object";
      final String IDENTIFIER = "object id";
      SimpleTestObject expectedSimpleTestObject = generateSimpleObjectObject(TITLE);
      List<String> names = new ArrayList<>();
      names.add("Riri");
      names.add("Fifi");
      names.add("Loulou");
      TestObjectWithNestedCollectionAndObjects expectedComplexObject = new TestObjectWithNestedCollectionAndObjects(IDENTIFIER);
      expectedComplexObject.setNames(names);
      expectedComplexObject.setIncludedObject(expectedSimpleTestObject);
      AnyContent anyContent = createAnyContentContainer("myTestObjectWithListAndObject", GITBAnyContentType.OBJECT);
      anyContent.getItem().add(createAnyContentValue("identifier", GITBAnyContentType.STRING, ValueEmbeddingEnumeration.STRING, IDENTIFIER));
      AnyContent anyContentNames = createAnyContentContainer("names", GITBAnyContentType.LIST);
      for (String name : names) {
         anyContentNames.getItem().add(createAnyContentValue(null, GITBAnyContentType.STRING, ValueEmbeddingEnumeration.STRING, name));
      }
      anyContent.getItem().add(anyContentNames);
      anyContent.getItem().add(generateSimpleObjectAnyContent("includedObject", TITLE));

      TestObjectWithNestedCollectionAndObjects objectWithListAndNestedObject = mapper
            .getObject(anyContent, TestObjectWithNestedCollectionAndObjects.class);

      Assertions.assertEquals(expectedComplexObject, objectWithListAndNestedObject,
            "The mapper must be able to decode complex object with hidden constructor");

   }

   /**
    * Verify an error is raised when trying to decode a constant.
    */
   @Test
   @Covers(requirements = {"GITB-72", "GITB-95"})
   public void testAnyContentToObjectConstantError() {
      AnyContent anyContent = createAnyContentContainer("constantObject", GITBAnyContentType.OBJECT);
      anyContent.getItem().add(createAnyContentValue("MY_CONSTANT", GITBAnyContentType.STRING, ValueEmbeddingEnumeration.STRING, ": it sucks..."));
      anyContent.getItem().add(createAnyContentValue("title", GITBAnyContentType.STRING, ValueEmbeddingEnumeration.STRING, "The great object"));

      Assertions.assertThrows(MappingException.class, () -> mapper.getObject(anyContent, WithConstantObject.class),
            "The mapper must raise an error if an AnyContent redefines a constant");
   }

   /**
    * Verify anyContent defined with an extended class for a field are well mapped.
    *
    * @throws MappingException in case of mapping error
    */
   @Test
   @Covers(requirements = {"GITB-74", "GITB-95"})
   public void testAnyContentExtendedFieldClass() throws MappingException {
      String testString = "TEST";
      TestExtendedObject testExtendedObject = new TestExtendedObject(32);
      testExtendedObject.setTestString(testString);
      TestObjectWithAbstractAttribute testObjectWithAbstractAttribute = new TestObjectWithAbstractAttribute(testExtendedObject);

      AnyContent anyContent = new MapperObjectToAnyContent().getAnyContent("TestObject", testObjectWithAbstractAttribute);

      TestObjectWithAbstractAttribute decodedObject = mapper.getObject(anyContent, TestObjectWithAbstractAttribute.class);

      Assertions.assertEquals(testObjectWithAbstractAttribute, decodedObject, "The mapper must be able to map extended class attributes.");
      Assertions.assertEquals(TestExtendedObject.class, decodedObject.getAttributeObject().getClass(),
              "The mapper must be able to map extended class attributes.");
      Assertions.assertEquals(testString, ((TestExtendedObject) decodedObject.getAttributeObject()).getTestString(),
              "The mapper must be able to map extended class attributes.");
   }

   /**
    * Generate an instance of {@link SimpleTestObject} for test
    *
    * @param title the string title of the instance.
    *
    * @return an instance of {@link SimpleTestObject}
    */
   private SimpleTestObject generateSimpleObjectObject(String title) {
      SimpleTestObject simpleTestObject = new SimpleTestObject();
      simpleTestObject.setTitle(title);
      simpleTestObject.setActivated(true);
      simpleTestObject.setIndex(23);
      return simpleTestObject;
   }

   /**
    * Generate the AnyContent equivalent of the test object generated with {@link #generateSimpleObjectObject(String)}
    *
    * @param anyContentName Name of the root element of the AnyContent.
    * @param title          title of the simple test object.
    *
    * @return
    */
   private AnyContent generateSimpleObjectAnyContent(String anyContentName, String title) {
      AnyContent anyContent = createAnyContentContainer(anyContentName, GITBAnyContentType.OBJECT);
      anyContent.getItem().add(createAnyContentValue("title", GITBAnyContentType.STRING, ValueEmbeddingEnumeration.STRING, title));
      anyContent.getItem().add(createAnyContentValue("activated", GITBAnyContentType.BOOLEAN, ValueEmbeddingEnumeration.STRING, "true"));
      anyContent.getItem().add(createAnyContentValue("index", GITBAnyContentType.NUMBER, ValueEmbeddingEnumeration.STRING, Integer.toString(23)));
      return anyContent;
   }

   /**
    * Shortcut method to create an {@link AnyContent} element with value.
    *
    * @param name            name of the AnyContent
    * @param type            type of the AnyContent
    * @param embeddingMethod embedding method of the AnyContent
    * @param value           value of the AnyContent
    *
    * @return an instance of AnyContent set with given parameters.
    */
   private AnyContent createAnyContentValue(String name, GITBAnyContentType type, ValueEmbeddingEnumeration embeddingMethod, String value) {
      AnyContent anyContent = new AnyContent();
      if (name != null) {
         anyContent.setName(name);
      }
      anyContent.setType(type.getGitbType());
      anyContent.setEmbeddingMethod(embeddingMethod);
      if (value != null) {
         anyContent.setValue(value);
      }
      return anyContent;
   }

   /**
    * Shortcut method to create an {@link AnyContent} element container, ready to receive items.
    *
    * @param name name of the AnyContent
    * @param type type of the AnyContent
    *
    * @return an instance of AnyContent set with given parameters.
    */
   private AnyContent createAnyContentContainer(String name, GITBAnyContentType type) {
      AnyContent anyContent = new AnyContent();
      if (name != null) {
         anyContent.setName(name);
      }
      anyContent.setType(type.getGitbType());
      return anyContent;
   }

}
