package net.ihe.gazelle.lib.gitbutils.adapter;

import com.gitb.core.AnyContent;
import com.gitb.core.ValueEmbeddingEnumeration;
import net.ihe.gazelle.lib.annotations.Covers;
import net.ihe.gazelle.lib.gitbutils.adapter.testobjects.*;
import org.junit.jupiter.api.Test;

import java.text.SimpleDateFormat;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test mapping from object to {@link AnyContent}
 */
@Covers(requirements = {"GITB-5", "GITB-31", "GITB-7"})
public class MapperObjectToAnyContentTest {

    /**
     * Tests mapping of a {@link String} object.
     *
     * @throws MappingException
     */
    @Test
    @Covers(requirements = {"GITB-10", "GITB-30"})
    public void getAnyContent_String() throws MappingException {
        String contentName = "Test";
        String contentValue = "testValue";
        MapperObjectToAnyContent mapper = new MapperObjectToAnyContent();

        AnyContent anyContent = mapper.getAnyContent(contentName, contentValue);

        checkPrimitiveAnyContent(anyContent, contentName, contentValue, GITBAnyContentType.STRING, ValueEmbeddingEnumeration.STRING);
    }

    /**
     * Test the mapping of null as an object.
     */
    @Test
    @Covers(requirements = {"GITB-28"})
    public void getAnyContent_Null() {
        String contentName = "Test";

        MapperObjectToAnyContent mapper = new MapperObjectToAnyContent();

        assertThrows(MappingException.class, () -> mapper.getAnyContent(contentName, null));
    }

    /**
     * Tests mapping of a boolean object.
     *
     * @throws MappingException
     */
    @Test
    @Covers(requirements = {"GITB-15", "GITB-83"})
    public void getAnyContent_boolean() throws MappingException {
        String contentName = "Test";
        boolean contentValue = true;
        MapperObjectToAnyContent mapper = new MapperObjectToAnyContent();

        AnyContent anyContent = mapper.getAnyContent(contentName, contentValue);

        checkPrimitiveAnyContent(anyContent, contentName, Boolean.toString(contentValue), GITBAnyContentType.BOOLEAN,
              ValueEmbeddingEnumeration.STRING);
    }

    /**
     * Tests mapping of a valued {@link Boolean} object.
     *
     * @throws MappingException
     */
    @Test
    @Covers(requirements = {"GITB-15", "GITB-84"})
    public void getAnyContent_Boolean_valued() throws MappingException {
        String contentName = "Test";
        Boolean contentValue = false;
        MapperObjectToAnyContent mapper = new MapperObjectToAnyContent();

        AnyContent anyContent = mapper.getAnyContent(contentName, contentValue);

        checkPrimitiveAnyContent(anyContent, contentName, contentValue.toString(), GITBAnyContentType.BOOLEAN, ValueEmbeddingEnumeration.STRING);
    }

    /**
     * Tests mapping of an int object.
     *
     * @throws MappingException
     */
    @Test
    @Covers(requirements = {"GITB-11", "GITB-75"})
    public void getAnyContent_int() throws MappingException {
        String contentName = "Test";
        int contentValue = 42;
        MapperObjectToAnyContent mapper = new MapperObjectToAnyContent();

        AnyContent anyContent = mapper.getAnyContent(contentName, contentValue);

        checkPrimitiveAnyContent(anyContent, contentName, Integer.toString(contentValue), GITBAnyContentType.NUMBER,
              ValueEmbeddingEnumeration.STRING);
    }

    /**
     * Tests mapping of a valued {@link Integer} object.
     *
     * @throws MappingException
     */
    @Test
    @Covers(requirements = {"GITB-11", "GITB-76"})
    public void getAnyContent_Integer_valued() throws MappingException {
        String contentName = "Test";
        Integer contentValue = 42;
        MapperObjectToAnyContent mapper = new MapperObjectToAnyContent();

        AnyContent anyContent = mapper.getAnyContent(contentName, contentValue);

        checkPrimitiveAnyContent(anyContent, contentName, contentValue.toString(), GITBAnyContentType.NUMBER, ValueEmbeddingEnumeration.STRING);
    }

    /**
     * Tests mapping of a long object.
     *
     * @throws MappingException
     */
    @Test
    @Covers(requirements = {"GITB-12", "GITB-77"})
    public void getAnyContent_long() throws MappingException {
        String contentName = "Test";
        long contentValue = 4269420;
        MapperObjectToAnyContent mapper = new MapperObjectToAnyContent();

        AnyContent anyContent = mapper.getAnyContent(contentName, contentValue);

        checkPrimitiveAnyContent(anyContent, contentName, Long.toString(contentValue), GITBAnyContentType.NUMBER, ValueEmbeddingEnumeration.STRING);
    }

    /**
     * Tests mapping of a valued {@link Long} object.
     *
     * @throws MappingException
     */
    @Test
    @Covers(requirements = {"GITB-12", "GITB-78"})
    public void getAnyContent_Long_valued() throws MappingException {
        String contentName = "Test";
        Long contentValue = Long.valueOf("4269420");
        MapperObjectToAnyContent mapper = new MapperObjectToAnyContent();

        AnyContent anyContent = mapper.getAnyContent(contentName, contentValue);

        checkPrimitiveAnyContent(anyContent, contentName, contentValue.toString(), GITBAnyContentType.NUMBER, ValueEmbeddingEnumeration.STRING);
    }

    /**
     * Tests mapping of a float object.
     *
     * @throws MappingException
     */
    @Test
    @Covers(requirements = {"GITB-13", "GITB-79"})
    public void getAnyContent_float() throws MappingException {
        String contentName = "Test";
        float contentValue = 3.14f;
        MapperObjectToAnyContent mapper = new MapperObjectToAnyContent();

        AnyContent anyContent = mapper.getAnyContent(contentName, contentValue);

        checkPrimitiveAnyContent(anyContent, contentName, Float.toString(contentValue), GITBAnyContentType.NUMBER, ValueEmbeddingEnumeration.STRING);
    }

    /**
     * Tests mapping of a valued {@link Float} object.
     *
     * @throws MappingException
     */
    @Test
    @Covers(requirements = {"GITB-13", "GITB-80"})
    public void getAnyContent_Float_valued() throws MappingException {
        String contentName = "Test";
        Float contentValue = 3.14f;
        MapperObjectToAnyContent mapper = new MapperObjectToAnyContent();

        AnyContent anyContent = mapper.getAnyContent(contentName, contentValue);

        checkPrimitiveAnyContent(anyContent, contentName, contentValue.toString(), GITBAnyContentType.NUMBER, ValueEmbeddingEnumeration.STRING);
    }

    /**
     * Tests mapping of a double object.
     *
     * @throws MappingException
     */
    @Test
    @Covers(requirements = {"GITB-14", "GITB-81"})
    public void getAnyContent_double() throws MappingException {
        String contentName = "Test";
        double contentValue = 3.14;
        MapperObjectToAnyContent mapper = new MapperObjectToAnyContent();

        AnyContent anyContent = mapper.getAnyContent(contentName, contentValue);

        checkPrimitiveAnyContent(anyContent, contentName, Double.toString(contentValue), GITBAnyContentType.NUMBER, ValueEmbeddingEnumeration.STRING);
    }

    /**
     * Tests mapping of a valued {@link Double} object.
     *
     * @throws MappingException
     */
    @Test
    @Covers(requirements = {"GITB-14", "GITB-82"})
    public void getAnyContent_Double_valued() throws MappingException {
        String contentName = "Test";
        Double contentValue = 3.14;
        MapperObjectToAnyContent mapper = new MapperObjectToAnyContent();

        AnyContent anyContent = mapper.getAnyContent(contentName, contentValue);

        checkPrimitiveAnyContent(anyContent, contentName, contentValue.toString(), GITBAnyContentType.NUMBER, ValueEmbeddingEnumeration.STRING);
    }

    /**
     * Tests mapping of a {@link List} object.
     *
     * @throws MappingException
     */
    @Test
    @Covers(requirements = {"GITB-20", "GITB-21", "GITB-89"})
    public void getAnyContent_List() throws MappingException {
        String contentName = "Test";
        List<Object> testList = new ArrayList<>();
        String stringListElement = "stringElement";
        double doubleListElement = 3.14;
        testList.add(stringListElement);
        testList.add(doubleListElement);
        MapperObjectToAnyContent mapper = new MapperObjectToAnyContent();

        AnyContent anyContent = mapper.getAnyContent(contentName, testList);

        checkAnyContent(anyContent, contentName, null, GITBAnyContentType.LIST, ValueEmbeddingEnumeration.BASE_64);
        assertEquals(2, anyContent.getItem().size());
        checkPrimitiveAnyContent(anyContent.getItem().get(0), ":" + stringListElement.getClass().getCanonicalName(), stringListElement, GITBAnyContentType.STRING, ValueEmbeddingEnumeration.STRING);
        checkPrimitiveAnyContent(anyContent.getItem().get(1), ":" + Double.class.getCanonicalName(), Double.toString(doubleListElement), GITBAnyContentType.NUMBER,
              ValueEmbeddingEnumeration.STRING);
    }

    /**
     * Tests mapping of a {@link Set} object.
     *
     * @throws MappingException
     */
    @Test
    @Covers(requirements = {"GITB-20", "GITB-21", "GITB-90"})
    public void getAnyContent_Set() throws MappingException {
        String contentName = "Test";
        Set<Object> testList = new HashSet<>();
        String stringListElement = "stringElement";
        double doubleListElement = 3.14;
        testList.add(stringListElement);
        testList.add(doubleListElement);
        MapperObjectToAnyContent mapper = new MapperObjectToAnyContent();

        AnyContent anyContent = mapper.getAnyContent(contentName, testList);

        checkAnyContent(anyContent, contentName, null, GITBAnyContentType.LIST, ValueEmbeddingEnumeration.BASE_64);
        assertEquals(2, anyContent.getItem().size());
        checkPrimitiveAnyContent(anyContent.getItem().get(0), ":" + stringListElement.getClass().getCanonicalName(), stringListElement, GITBAnyContentType.STRING, ValueEmbeddingEnumeration.STRING);
        checkPrimitiveAnyContent(anyContent.getItem().get(1), ":" + Double.class.getCanonicalName(), Double.toString(doubleListElement), GITBAnyContentType.NUMBER,
              ValueEmbeddingEnumeration.STRING);
    }

    /**
     * Tests mapping of a {@link Date} object.
     *
     * @throws MappingException
     */
    @Test
    @Covers(requirements = {"GITB-17", "GITB-85"})
    public void getAnyContent_Date() throws MappingException {
        String contentName = "Test";
        Date contentValue = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat(MappingConstant.DATE_FORMAT);
        String stringDate = dateFormat.format(contentValue);
        MapperObjectToAnyContent mapper = new MapperObjectToAnyContent();

        AnyContent anyContent = mapper.getAnyContent(contentName, contentValue);

        checkPrimitiveAnyContent(anyContent, contentName, stringDate, GITBAnyContentType.STRING, ValueEmbeddingEnumeration.STRING);
    }

    /**
     * Tests mapping of a valued {@link Boolean} object.
     *
     * @throws MappingException
     */
    @Test
    @Covers(requirements = {"GITB-35", "GITB-57"})
    public void getAnyContent_Enum() throws MappingException {
        String contentName = "Test";
        GITBAnyContentType contentValue = GITBAnyContentType.STRING;
        MapperObjectToAnyContent mapper = new MapperObjectToAnyContent();

        AnyContent anyContent = mapper.getAnyContent(contentName, contentValue);

        checkPrimitiveAnyContent(anyContent, contentName, contentValue.name(), GITBAnyContentType.STRING, ValueEmbeddingEnumeration.STRING);
    }

    /**
     * Tests mapping of an object with a {@link String} attribute.
     *
     * @throws MappingException
     */
    @Test
    @Covers(requirements = {"GITB-24", "GITB-10", "GITB-8", "GITB-9"})
    public void getAnyContent_Object_String() throws MappingException {
        String contentName = "Test";
        String testString = "Tarte aux pommes";
        TestObject contentValue = new TestObject(testString, null, null, null, null, null, null);
        MapperObjectToAnyContent mapper = new MapperObjectToAnyContent();

        AnyContent anyContent = mapper.getAnyContent(contentName, contentValue);

        checkAnyContent(anyContent, contentName, null, GITBAnyContentType.OBJECT, ValueEmbeddingEnumeration.BASE_64);
        List<AnyContent> item = anyContent.getItem();
        assertEquals(1, item.size());
        checkPrimitiveAnyContent(item.get(0), "testString", testString, GITBAnyContentType.STRING, ValueEmbeddingEnumeration.STRING);
    }

    /**
     * Tests mapping of an object with a {@link Boolean} attribute.
     *
     * @throws MappingException
     */
    @Test
    @Covers(requirements = {"GITB-24", "GITB-15", "GITB-8", "GITB-9"})
    public void getAnyContent_Object_Boolean() throws MappingException {
        String contentName = "Test";
        Boolean testBoolean = false;
        TestObject contentValue = new TestObject(null, testBoolean, null, null, null, null, null);
        MapperObjectToAnyContent mapper = new MapperObjectToAnyContent();

        AnyContent anyContent = mapper.getAnyContent(contentName, contentValue);

        checkAnyContent(anyContent, contentName, null, GITBAnyContentType.OBJECT, ValueEmbeddingEnumeration.BASE_64);
        List<AnyContent> item = anyContent.getItem();
        assertEquals(1, item.size());
        checkPrimitiveAnyContent(item.get(0), "testBoolean", Boolean.toString(testBoolean), GITBAnyContentType.BOOLEAN,
              ValueEmbeddingEnumeration.STRING);
    }

    /**
     * Tests mapping of an object with an {@link Integer} attribute.
     *
     * @throws MappingException
     */
    @Test
    @Covers(requirements = {"GITB-24", "GITB-11", "GITB-8", "GITB-9"})
    public void getAnyContent_Object_Integer() throws MappingException {
        String contentName = "Test";
        int testInt = 42;
        TestObject contentValue = new TestObject(null, null, testInt, null, null, null, null);
        MapperObjectToAnyContent mapper = new MapperObjectToAnyContent();

        AnyContent anyContent = mapper.getAnyContent(contentName, contentValue);

        checkAnyContent(anyContent, contentName, null, GITBAnyContentType.OBJECT, ValueEmbeddingEnumeration.BASE_64);
        List<AnyContent> item = anyContent.getItem();
        assertEquals(1, item.size());
        checkPrimitiveAnyContent(item.get(0), "testInt", Integer.toString(testInt), GITBAnyContentType.NUMBER, ValueEmbeddingEnumeration.STRING);
    }

    /**
     * Tests mapping of an object with a {@link List} attribute.
     *
     * @throws MappingException
     */
    @Test
    @Covers(requirements = {"GITB-24", "GITB-20", "GITB-21", "GITB-8", "GITB-9"})
    public void getAnyContent_Object_list() throws MappingException {
        String contentName = "Test";
        List<String> testList = new ArrayList<>();
        testList.add(contentName);
        testList.add("testString");
        TestObject contentValue = new TestObject(null, null, null, testList, null, null, null);
        MapperObjectToAnyContent mapper = new MapperObjectToAnyContent();

        AnyContent anyContent = mapper.getAnyContent(contentName, contentValue);

        checkAnyContent(anyContent, contentName, null, GITBAnyContentType.OBJECT, ValueEmbeddingEnumeration.BASE_64);
        List<AnyContent> item = anyContent.getItem();
        assertEquals(1, item.size());
        checkAnyContent(item.get(0), "testList", null, GITBAnyContentType.LIST, ValueEmbeddingEnumeration.BASE_64);
        checkPrimitiveAnyContent(item.get(0).getItem().get(0), ":" + contentName.getClass().getCanonicalName(), contentName, GITBAnyContentType.STRING, ValueEmbeddingEnumeration.STRING);
        checkPrimitiveAnyContent(item.get(0).getItem().get(1), ":" + String.class.getCanonicalName(), "testString", GITBAnyContentType.STRING, ValueEmbeddingEnumeration.STRING);
    }

    /**
     * Tests mapping of an object with a {@link List} attribute filled with other objects.
     *
     * @throws MappingException
     */
    @Test
    @Covers(requirements = {"GITB-24", "GITB-20", "GITB-21", "GITB-8", "GITB-9"})
    public void getAnyContent_Object_list_Object() throws MappingException {
        String contentName = "Test";
        List<TestAttributeObject> testList = new ArrayList<>();
        TestAttributeObject attributeObject1 = new TestAttributeObject(320);
        TestAttributeObject attributeObject2 = new TestAttributeObject(43);
        TestAttributeObject attributeObject3 = new TestAttributeObject(32, attributeObject2);
        testList.add(attributeObject1);
        testList.add(attributeObject3);
        MapperObjectToAnyContent mapper = new MapperObjectToAnyContent();

        AnyContent anyContent = mapper.getAnyContent(contentName, testList);

        checkAnyContent(anyContent, contentName, null, GITBAnyContentType.LIST, ValueEmbeddingEnumeration.BASE_64);
        assertEquals(2, anyContent.getItem().size(), "Size of item list is expected to be 2, as the list contains two objects.");
        checkAnyContent(anyContent.getItem().get(0), ":" + attributeObject1.getClass().getCanonicalName(), null, GITBAnyContentType.OBJECT, ValueEmbeddingEnumeration.BASE_64);
        assertEquals(1, anyContent.getItem().get(0).getItem().size(), "The object shall only have 1 item as he only contains an ID.");
        checkPrimitiveAnyContent(anyContent.getItem().get(0).getItem().get(0), "id", Integer.toString(attributeObject1.getId()),
              GITBAnyContentType.NUMBER, ValueEmbeddingEnumeration.STRING);
        checkAnyContent(anyContent.getItem().get(1), ":" + attributeObject3.getClass().getCanonicalName(), null, GITBAnyContentType.OBJECT, ValueEmbeddingEnumeration.BASE_64);
        assertEquals(2, anyContent.getItem().get(1).getItem().size(), "The object shall only have 2 item as he contains an ID and a child.");
        checkPrimitiveAnyContent(anyContent.getItem().get(1).getItem().get(0), "id", Integer.toString(attributeObject3.getId()),
              GITBAnyContentType.NUMBER, ValueEmbeddingEnumeration.STRING);
        checkAnyContent(anyContent.getItem().get(1).getItem().get(1), "child", null, GITBAnyContentType.OBJECT, ValueEmbeddingEnumeration.BASE_64);
    }

    /**
     * Tests mapping of an object with a {@link Date} attribute.
     *
     * @throws MappingException
     */
    @Test
    @Covers(requirements = {"GITB-24", "GITB-17", "GITB-8", "GITB-9"})
    public void getAnyContent_Object_Date() throws MappingException {
        String contentName = "Test";
        Date testDate = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat(MappingConstant.DATE_FORMAT);
        String stringDate = dateFormat.format(testDate);
        TestObject contentValue = new TestObject(null, null, null, null, testDate, null, null);
        MapperObjectToAnyContent mapper = new MapperObjectToAnyContent();

        AnyContent anyContent = mapper.getAnyContent(contentName, contentValue);

        checkAnyContent(anyContent, contentName, null, GITBAnyContentType.OBJECT, ValueEmbeddingEnumeration.BASE_64);
        List<AnyContent> item = anyContent.getItem();
        assertEquals(1, item.size());
        checkPrimitiveAnyContent(item.get(0), "testDate", stringDate, GITBAnyContentType.STRING, ValueEmbeddingEnumeration.STRING);
    }

    /**
     * Tests mapping of an object with an {@link Enum} attribute.
     *
     * @throws MappingException
     */
    @Test
    @Covers(requirements = {"GITB-24", "GITB-10", "GITB-8", "GITB-9", "GITB-35", "GITB-57"})
    public void getAnyContent_Object_Enum() throws MappingException {
        String contentName = "Test";
        TestObject contentValue = new TestObject(null, null, null, null, null, GITBAnyContentType.NUMBER, null);
        MapperObjectToAnyContent mapper = new MapperObjectToAnyContent();

        AnyContent anyContent = mapper.getAnyContent(contentName, contentValue);

        checkAnyContent(anyContent, contentName, null, GITBAnyContentType.OBJECT, ValueEmbeddingEnumeration.BASE_64);
        List<AnyContent> item = anyContent.getItem();
        assertEquals(1, item.size());
        checkPrimitiveAnyContent(item.get(0), "testEnum", GITBAnyContentType.NUMBER.name(), GITBAnyContentType.STRING,
              ValueEmbeddingEnumeration.STRING);
    }

    /**
     * Tests mapping of an object with another object attribute.
     *
     * @throws MappingException
     */
    @Test
    @Covers(requirements = {"GITB-24", "GITB-8", "GITB-9"})
    public void getAnyContent_Object_Object() throws MappingException {
        String contentName = "Test";
        TestAttributeObject attributeObject = new TestAttributeObject(34);
        TestObject contentValue = new TestObject(null, null, null, null, null, null, attributeObject);
        MapperObjectToAnyContent mapper = new MapperObjectToAnyContent();

        AnyContent anyContent = mapper.getAnyContent(contentName, contentValue);

        checkAnyContent(anyContent, contentName, null, GITBAnyContentType.OBJECT, ValueEmbeddingEnumeration.BASE_64);
        List<AnyContent> item = anyContent.getItem();
        assertEquals(1, item.size());
        checkAnyContent(item.get(0), "attributeObject", null, GITBAnyContentType.OBJECT, ValueEmbeddingEnumeration.BASE_64);
        checkPrimitiveAnyContent(item.get(0).getItem().get(0), "id", "34", GITBAnyContentType.NUMBER, ValueEmbeddingEnumeration.STRING);
    }

    /**
     * Tests mapping of an object with a recursive object attribute.
     *
     * @throws MappingException
     */
    @Test
    @Covers(requirements = {"GITB-24", "GITB-8", "GITB-9"})
    public void getAnyContent_Object_Object_Recursive() throws MappingException {
        String contentName = "Test";
        TestAttributeObject grandChildAttributeObject = new TestAttributeObject(34);
        TestAttributeObject childAttributeObject = new TestAttributeObject(33, grandChildAttributeObject);
        TestAttributeObject attributeObject = new TestAttributeObject(32, childAttributeObject);
        TestObject contentValue = new TestObject(null, null, null, null, null, null, attributeObject);
        MapperObjectToAnyContent mapper = new MapperObjectToAnyContent();

        AnyContent anyContent = mapper.getAnyContent(contentName, contentValue);

        checkAnyContent(anyContent, contentName, null, GITBAnyContentType.OBJECT, ValueEmbeddingEnumeration.BASE_64);
        List<AnyContent> item = anyContent.getItem();
        assertEquals(1, item.size());
        checkAnyContent(item.get(0), "attributeObject", null, GITBAnyContentType.OBJECT, ValueEmbeddingEnumeration.BASE_64);
        checkPrimitiveAnyContent(item.get(0).getItem().get(0), "id", "32", GITBAnyContentType.NUMBER, ValueEmbeddingEnumeration.STRING);
        assertEquals(2, item.get(0).getItem().size());
        checkAnyContent(item.get(0).getItem().get(1), "child", null, GITBAnyContentType.OBJECT, ValueEmbeddingEnumeration.BASE_64);
        checkPrimitiveAnyContent(item.get(0).getItem().get(1).getItem().get(0), "id", "33", GITBAnyContentType.NUMBER,
              ValueEmbeddingEnumeration.STRING);
        assertEquals(2, item.get(0).getItem().get(1).getItem().size());
        checkAnyContent(item.get(0).getItem().get(1).getItem().get(1), "child", null, GITBAnyContentType.OBJECT, ValueEmbeddingEnumeration.BASE_64);
        checkPrimitiveAnyContent(item.get(0).getItem().get(1).getItem().get(1).getItem().get(0), "id", "34", GITBAnyContentType.NUMBER,
              ValueEmbeddingEnumeration.STRING);
    }

    /**
     * Tests mapping of an object with a combination of multiple types of attribute.
     *
     * @throws MappingException
     */
    @Test
    @Covers(requirements = {"GITB-24", "GITB-8"})
    public void getAnyContent_FullObject() throws MappingException {
        String contentName = "Test";
        String testString = "Tarte aux pommes";
        Boolean testBoolean = false;
        int testInt = 42;
        List<String> testList = new ArrayList<>();
        testList.add(contentName);
        testList.add(testString);
        Date testDate = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat(MappingConstant.DATE_FORMAT);
        String stringDate = dateFormat.format(testDate);
        TestAttributeObject attributeObject = new TestAttributeObject(34);
        TestObject contentValue = new TestObject(testString, testBoolean, testInt, testList, testDate, GITBAnyContentType.NUMBER, attributeObject);
        MapperObjectToAnyContent mapper = new MapperObjectToAnyContent();

        AnyContent anyContent = mapper.getAnyContent(contentName, contentValue);

        checkAnyContent(anyContent, contentName, null, GITBAnyContentType.OBJECT, ValueEmbeddingEnumeration.BASE_64);
        List<AnyContent> item = anyContent.getItem();
        checkPrimitiveAnyContent(item.get(0), "testString", testString, GITBAnyContentType.STRING, ValueEmbeddingEnumeration.STRING);
        checkPrimitiveAnyContent(item.get(1), "testBoolean", Boolean.toString(testBoolean), GITBAnyContentType.BOOLEAN,
              ValueEmbeddingEnumeration.STRING);
        checkPrimitiveAnyContent(item.get(2), "testInt", Integer.toString(testInt), GITBAnyContentType.NUMBER, ValueEmbeddingEnumeration.STRING);
        checkAnyContent(item.get(3), "testList", null, GITBAnyContentType.LIST, ValueEmbeddingEnumeration.BASE_64);
        checkPrimitiveAnyContent(item.get(3).getItem().get(0), ":" + contentName.getClass().getCanonicalName(), contentName, GITBAnyContentType.STRING, ValueEmbeddingEnumeration.STRING);
        checkPrimitiveAnyContent(item.get(3).getItem().get(1), ":" + testString.getClass().getCanonicalName(), testString, GITBAnyContentType.STRING, ValueEmbeddingEnumeration.STRING);
        checkPrimitiveAnyContent(item.get(4), "testDate", stringDate, GITBAnyContentType.STRING, ValueEmbeddingEnumeration.STRING);
        checkPrimitiveAnyContent(item.get(5), "testEnum", GITBAnyContentType.NUMBER.name(), GITBAnyContentType.STRING,
              ValueEmbeddingEnumeration.STRING);
        checkAnyContent(item.get(6), "attributeObject", null, GITBAnyContentType.OBJECT, ValueEmbeddingEnumeration.BASE_64);
        checkPrimitiveAnyContent(item.get(6).getItem().get(0), "id", "34", GITBAnyContentType.NUMBER, ValueEmbeddingEnumeration.STRING);
    }

    /**
     * Tests mapping of an object with fields comming from a superclass.
     *
     * @throws MappingException
     */
    @Test
    @Covers(requirements = {"GITB-24", "GITB-8", "GITB-9"})
    public void getAnyContent_extended_Object() throws MappingException {
        String contentName = "Test";
        String contentValue = "Test";
        int id = 34;
        TestExtendedObject testExtendedObject = new TestExtendedObject(id);
        testExtendedObject.setTestString(contentName);
        MapperObjectToAnyContent mapper = new MapperObjectToAnyContent();

        AnyContent anyContent = mapper.getAnyContent(contentName, testExtendedObject);

        checkAnyContent(anyContent, contentName, null, GITBAnyContentType.OBJECT, ValueEmbeddingEnumeration.BASE_64);
        List<AnyContent> item = anyContent.getItem();
        assertEquals(2, item.size());
        checkPrimitiveAnyContent(item.get(0), "testString", contentValue, GITBAnyContentType.STRING, ValueEmbeddingEnumeration.STRING);
        checkPrimitiveAnyContent(item.get(1), "id", Integer.toString(id), GITBAnyContentType.NUMBER, ValueEmbeddingEnumeration.STRING);
    }

    /**
     * Check that static final constants are not transformed
     */
    @Test
    @Covers(requirements = "GITB-71")
    public void getAnyContent_with_constant() throws MappingException{
        String contentName = "Test";
        String contentValue = "Test";
        WithConstantObject withConstantObject = new WithConstantObject();
        withConstantObject.setTitle(contentValue);
        MapperObjectToAnyContent mapper = new MapperObjectToAnyContent();

        AnyContent anyContent = mapper.getAnyContent(contentName, withConstantObject);

        checkAnyContent(anyContent, contentName, null, GITBAnyContentType.OBJECT, ValueEmbeddingEnumeration.BASE_64);
        List<AnyContent> item = anyContent.getItem();
        assertEquals(1, item.size());
        checkPrimitiveAnyContent(item.get(0), "title", contentValue, GITBAnyContentType.STRING, ValueEmbeddingEnumeration.STRING);
    }

    /**
     * Check that abstract attribute are transformed according to the actual concrete class.
     */
    @Test
    @Covers(requirements = "GITB-73")
    public void getAnyContent_with_abstract_attribute() throws MappingException{
        String contentName = "Test";
        String contentValue = "Test";
        TestExtendedObject testExtendedObject = new TestExtendedObject(32);
        testExtendedObject.setTestString(contentValue);
        TestObjectWithAbstractAttribute testObjectWithAbstractAttribute = new TestObjectWithAbstractAttribute(testExtendedObject);
        MapperObjectToAnyContent mapper = new MapperObjectToAnyContent();

        AnyContent anyContent = mapper.getAnyContent(contentName, testObjectWithAbstractAttribute);

        checkAnyContent(anyContent, contentName, null, GITBAnyContentType.OBJECT, ValueEmbeddingEnumeration.BASE_64);
        List<AnyContent> item = anyContent.getItem();
        assertEquals(1, item.size());
        checkAnyContent(item.get(0), "attributeObject:net.ihe.gazelle.lib.gitbutils.adapter.testobjects.TestExtendedObject",
                null, GITBAnyContentType.OBJECT, ValueEmbeddingEnumeration.BASE_64);
        assertEquals(2, item.get(0).getItem().size());
        checkAnyContent(item.get(0).getItem().get(0), "testString", contentValue, GITBAnyContentType.STRING, ValueEmbeddingEnumeration.STRING);
        checkAnyContent(item.get(0).getItem().get(1), "id", "32", GITBAnyContentType.NUMBER, ValueEmbeddingEnumeration.STRING);

    }

    /**
     * Check all values in a {@link AnyContent} object corresponding to a primitive type (according to GITB)
     */
    private void checkPrimitiveAnyContent(AnyContent anyContent, String contentName, String contentValue, GITBAnyContentType gitbType,
                                          ValueEmbeddingEnumeration embeddingMethod) {
        checkAnyContent(anyContent, contentName, contentValue, gitbType, embeddingMethod);
        assertTrue(anyContent.getItem().isEmpty(), "The list of item for primitive AnyContent shall be empty.");
    }

    /**
     * Check all values in a {@link AnyContent} object, except the list of items.
     */
    @Covers(requirements = "GITB-25")
    private void checkAnyContent(AnyContent anyContent, String contentName, String contentValue, GITBAnyContentType gitbType,
                                 ValueEmbeddingEnumeration embeddingMethod) {
        assertNotNull(anyContent);
        assertEquals(contentName, anyContent.getName());
        assertEquals(contentValue, anyContent.getValue());
        assertEquals(gitbType.getGitbType(), anyContent.getType());
        assertEquals(embeddingMethod, anyContent.getEmbeddingMethod());
        assertNull(anyContent.getEncoding());
    }
}
