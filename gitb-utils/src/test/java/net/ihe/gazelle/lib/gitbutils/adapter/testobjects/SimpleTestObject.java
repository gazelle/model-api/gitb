package net.ihe.gazelle.lib.gitbutils.adapter.testobjects;

import java.util.Objects;

/**
 * Simple object used for test purposes
 */
public class SimpleTestObject {
   private String title;
   private boolean activated;
   private int index;

   /**
    * Getter for the title property
    * @return the value of the property
    */
   public String getTitle() {
      return title;
   }

   /**
    * Setter for the title property
    * @param title   the value to set to the property
    */
   public void setTitle(String title) {
      this.title = title;
   }

   /**
    * Getter for the active property
    * @return the value of the property
    */
   public boolean isActivated() {
      return activated;
   }

   /**
    * Setter for the active property
    * @param activated   the value to set to the property
    */
   public void setActivated(boolean activated) {
      this.activated = activated;
   }

   /**
    * Getter for the index property
    * @return the value of the property
    */
   public int getIndex() {
      return index;
   }

   /**
    * Setter for the index property
    * @param index  the value to set to the property
    */
   public void setIndex(int index) {
      this.index = index;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public boolean equals(Object o) {
      if (this == o) {
         return true;
      }
      if (o == null || getClass() != o.getClass()) {
         return false;
      }
      SimpleTestObject that = (SimpleTestObject) o;
      return activated == that.activated &&
            index == that.index &&
            Objects.equals(title, that.title);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public int hashCode() {
      return Objects.hash(title, activated, index);
   }
}
