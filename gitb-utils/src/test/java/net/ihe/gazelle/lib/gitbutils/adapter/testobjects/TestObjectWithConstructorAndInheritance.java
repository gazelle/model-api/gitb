package net.ihe.gazelle.lib.gitbutils.adapter.testobjects;

import java.util.Date;

/**
 * Test object for case where object has its own constructor in addition to those defined by the superclass.
 */
public class TestObjectWithConstructorAndInheritance extends SimpleTestObject {

   private String identifier;
   private Date updateTime;

   /**
    * Constructor for the class.
    * @param identifier     value of the identifier property to set
    */
   public TestObjectWithConstructorAndInheritance(String identifier) {
      this.identifier = identifier;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public void setTitle(String title) {
      super.setTitle(title);
      updateTime = new Date();
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public void setActivated(boolean activated) {
      super.setActivated(activated);
      updateTime = new Date();
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public void setIndex(int index) {
      super.setIndex(index);
      updateTime = new Date();
   }

   /**
    * Getter for the identifier property
    * @return the value of the property
    */
   public String getIdentifier() {
      return identifier;
   }

   /**
    * Getter for the updateTime property
    * @return the value of the property
    */
   public Date getUpdateTime() {
      return updateTime;
   }

}
