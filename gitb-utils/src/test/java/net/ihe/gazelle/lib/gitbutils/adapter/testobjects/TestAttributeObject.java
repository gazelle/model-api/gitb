package net.ihe.gazelle.lib.gitbutils.adapter.testobjects;

import java.util.Objects;

/**
 * Test object aimed at being an attribute of a more complex object to test recursions and object inclusions.
 */
public class TestAttributeObject {
    private int id;

    private TestAttributeObject child;

    /**
     * Constructor initializing a new object with an ID.
     * @param id        value of the id property to set
     */
    public TestAttributeObject(int id) {
        this.id = id;
    }

    /**
     * Constructor initializing a new object with an ID and a Child.
     * @param id         value of the id property to set
     * @param child      value of the child property to set
     */
    public TestAttributeObject(int id, TestAttributeObject child) {
        this.id = id;
        this.child = child;
    }

    /**
     * Getter for the id property
     * @return value of the id property
     */
    public int getId() {
        return id;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TestAttributeObject that = (TestAttributeObject) o;
        return id == that.id &&
              Objects.equals(child, that.child);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hash(id, child);
    }
}
