package net.ihe.gazelle.lib.gitbutils.adapter.testobjects;

/**
 * Test Enum to ensure the mapping is correctly done for Enumerations from and to GITB AnyContent objects.
 */
public enum TestEnum {

   VALUE1,
   VALUE2,
   VALUE3;
}
