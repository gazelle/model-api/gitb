package net.ihe.gazelle.lib.gitbutils.adapter;

import com.gitb.core.AnyContent;
import net.ihe.gazelle.lib.annotations.Covers;
import net.ihe.gazelle.lib.gitbutils.adapter.testobjects.TestAttributeObject;
import net.ihe.gazelle.lib.gitbutils.adapter.testobjects.TestObject;
import net.ihe.gazelle.lib.gitbutils.adapter.testobjects.TestObjectAccessWithoutGetterSetter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Verify the mapper is consistent in both transformation direction. End to end test.
 */
@Covers(requirements = {"GITB-6", "GITB-5"})
public class MapperInOutEqualityTest {

   private MapperObjectToAnyContent objectToAnyContent = new MapperObjectToAnyContent();
   private MapperAnyContentToObject anyContentToObject = new MapperAnyContentToObject();

   /**
    * Verify the mapper is able to decode an object it has itself encoded as AnyContent on a complex Object.
    *
    * @throws MappingException in case of mapping error
    */
   @Test
   @Covers(requirements = {"GITB-5", "GITB-6"})
   public void patient_to_AnyContent_and_back_equality() throws MappingException {
      String contentName = "Test";
      String testString = "Tarte aux pommes";
      Boolean testBoolean = false;
      int testInt = 42;
      List<String> testList = new ArrayList<>();
      testList.add(contentName);
      testList.add(testString);
      Date testDate = new Date();
      TestAttributeObject attributeObject = new TestAttributeObject(34);
      TestObject expectedObject = new TestObject(testString, testBoolean, testInt, testList, testDate, GITBAnyContentType.NUMBER, attributeObject);

      AnyContent anyContentTest = objectToAnyContent.getAnyContent("testObject", expectedObject);
      TestObject mappedObject = anyContentToObject.getObject(anyContentTest, TestObject.class);

      Assertions.assertEquals(expectedObject, mappedObject, "Mapped Object shall be identical to the initial Object !");
   }

   /**
    * Verify the mapper is able to decode an object it has itself encoded as AnyContent, without using Getter or Setter for properties.
    *
    * @throws MappingException in case of mapping error
    */
   @Test
   @Covers(requirements = "GITB-34")
   public void testGetterAndSetterNotUsed() throws MappingException {
      TestObjectAccessWithoutGetterSetter expectedObject = new TestObjectAccessWithoutGetterSetter("Test");

      AnyContent anyContentTest = objectToAnyContent.getAnyContent("testObject", expectedObject);
      TestObjectAccessWithoutGetterSetter mappedObject = anyContentToObject.getObject(anyContentTest, TestObjectAccessWithoutGetterSetter.class);

      assertTrue(expectedObject.equals(mappedObject), "Mapped Object shall be identical to the initial Object !");
   }
}
