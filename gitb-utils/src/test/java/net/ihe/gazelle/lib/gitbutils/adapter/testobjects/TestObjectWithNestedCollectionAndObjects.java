package net.ihe.gazelle.lib.gitbutils.adapter.testobjects;

import net.ihe.gazelle.lib.annotations.Package;

import java.util.List;
import java.util.Objects;

/**
 * Test object with nested collections and objects.
 */
public class TestObjectWithNestedCollectionAndObjects {

   private String identifier;
   private List<String> names;
   private SimpleTestObject includedObject;

   /**
    * Hidden package constructor to be used by the Mapper
    */
   @Package
   TestObjectWithNestedCollectionAndObjects() {
   }

   /**
    * Public constructor, but arguments name does not match fields, must not be used by the mapper.
    *
    * @param objectId
    */
   public TestObjectWithNestedCollectionAndObjects(String objectId) {
      this.identifier = objectId;
   }

   /**
    * Getter for the identifier property
    * @return the value of the property
    */
   public String getIdentifier() {
      return identifier;
   }

   /**
    * Getter for the names property
    * @return the value of the property
    */
   public List<String> getNames() {
      return names;
   }

   /**
    * Setter for the names property
    * @param names  the value of the property to set
    */
   public void setNames(List<String> names) {
      this.names = names;
   }

   /**
    * Getter for the includedObject property
    * @return the value of the property
    */
   public SimpleTestObject getIncludedObject() {
      return includedObject;
   }

   /**
    * Setter for the includedObject property
    * @param includedObject  the value of the property to set
    */
   public void setIncludedObject(SimpleTestObject includedObject) {
      this.includedObject = includedObject;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public boolean equals(Object o) {
      if (this == o) {
         return true;
      }
      if (o == null || getClass() != o.getClass()) {
         return false;
      }
      TestObjectWithNestedCollectionAndObjects that = (TestObjectWithNestedCollectionAndObjects) o;
      return Objects.equals(identifier, that.identifier) &&
            Objects.equals(names, that.names) &&
            Objects.equals(includedObject, that.includedObject);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public int hashCode() {
      return Objects.hash(identifier, names, includedObject);
   }
}
