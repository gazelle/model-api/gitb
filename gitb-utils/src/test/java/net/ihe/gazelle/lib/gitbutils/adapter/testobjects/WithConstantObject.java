package net.ihe.gazelle.lib.gitbutils.adapter.testobjects;

/**
 * Test object with a constant. Tests that constant fields are ignored by the mapping.
 */
public class WithConstantObject {

   private static final String MY_CONSTANT = ": It rocks!";
   private String title;

   /**
    * Getter for the title property
    * @return the value of the property concatenated with the MY_CONSTANT value.
    */
   public String getTitle() {
      return title + MY_CONSTANT;
   }

   /**
    * Setter for the title property
    * @param title   value to set to the title property
    */
   public void setTitle(String title) {
      this.title = title;
   }
}
