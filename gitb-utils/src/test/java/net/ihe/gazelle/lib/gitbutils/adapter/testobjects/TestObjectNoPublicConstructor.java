package net.ihe.gazelle.lib.gitbutils.adapter.testobjects;

/**
 * Test Object with no public constructor
 */
public class TestObjectNoPublicConstructor {

    private String test;

    /**
     * Private constructor to hide implicit public one.
     */
    private TestObjectNoPublicConstructor() {
    }

    /**
     * Private constructor.
     *
     * @param test value of the test property
     */
    private TestObjectNoPublicConstructor(String test) {
        this.test = test;
    }
}
