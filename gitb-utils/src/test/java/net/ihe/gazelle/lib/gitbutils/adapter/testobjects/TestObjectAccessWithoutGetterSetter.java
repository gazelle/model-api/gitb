package net.ihe.gazelle.lib.gitbutils.adapter.testobjects;

import static org.junit.jupiter.api.Assertions.fail;

/**
 * Test Object that makes a test fail if it uses its Getter of Setter
 */
public class TestObjectAccessWithoutGetterSetter {

    private String test;

    /**
     * Public constructor to avoid setting properties
     *
     * @param test value of the test property
     */
    public TestObjectAccessWithoutGetterSetter(String test) {
        this.test = test;
    }

    /**
     * Getter for the test property
     *
     * @return the value of the test property
     */
    public String getTest() {
        fail("Getter for properties should not be called");
        return test;
    }

    /**
     * Setter for the test property
     *
     * @param test value to set to the property
     */
    public void setTest(String test) {
        fail("Setter for properties should not be called");
        this.test = test;
    }

    /**
     * Checks if two objects are equals
     *
     * @param testObjectAccessWithoutGetterSetter object to check against the current instance
     * @return true if both objects are equals, false otherwise
     */
    public boolean equals(TestObjectAccessWithoutGetterSetter testObjectAccessWithoutGetterSetter) {
        return this.test.equals(testObjectAccessWithoutGetterSetter.test);
    }
}
